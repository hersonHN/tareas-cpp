#ifndef __LECTOR_H__
#define __LECTOR_H__

#include <iostream>
#include <vector>
#include <string.h>
#include "general.cpp"
#include "terminal.cpp"
#include "Matriz/Matriz.cpp"



using namespace std;



class Lector {
    private:
    
    int filas, columnas;
    int filaActiva, columnaActiva;
    
    double leerPosicion();
    int leerMovimiento();
    void imprimirHilight(string);
    void procesarMovimiento(int);
    
    public:
    
    int espacios;
    Matriz *matriz;
    
    void __initialize() {
        espacios = 10;
        filaActiva = 0;
        columnaActiva = 0;
    }
    
    Lector() {
        filas = columnas = 0;
        matriz = new Matriz(filas, columnas);
        __initialize();
    }
    
    Lector(Matriz &m) {
        
        filas = m.getFilas();
        columnas = m.getColumnas();
        matriz = &m;
        __initialize();
    }
    
    Lector(int f, int c) {
        filas = f;
        columnas = c;
        matriz = new Matriz(filas, columnas);
        __initialize();
    }
    
    ~Lector() {
    //    delete matriz;
    }
    
    void leer();
    void imprimir();
};
    




#endif
