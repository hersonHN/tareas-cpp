#ifndef __LECTOR_CPP__
#define __LECTOR_CPP__


#include <iostream>
#include <string.h>
#include "Lector.h"


using namespace std;


void Lector::leer() {

    int movimiento = 0;
    string activo;
    
    do {
        imprimir();
        
        activo = matriz->formatearNumero(filaActiva, columnaActiva);
        imprimirHilight(activo);
        
        movimiento = leerMovimiento();
        procesarMovimiento(movimiento);
        
    } while (movimiento != __ESC__);
    
}


void Lector::imprimir() {
    gotoxy(0, 0);
    cout << endl;
    cout << "Valores de la matriz: " << endl << endl;
    cout << matriz->toString() << endl;
    cout << string(200, ' ');
}


void Lector::imprimirHilight(string texto) {
    gotoxy(filaActiva + 3, columnaActiva * espacios + 1);
    color(texto, 14, 0);
}


int Lector::leerMovimiento() {
    // Informacion de movimientos
    gotoxy(filas + 4, 0);
    cout << "Use las flechas para moverse, Enter para modificar, Esc para salir";
    
    return getch();
}


void Lector::procesarMovimiento(int movimiento) {
    if (movimiento == __AR__) {
        if (filaActiva > 0)
            filaActiva--;
    } else
    if (movimiento == __AB__) {
        if (filaActiva < filas - 1)
            filaActiva++;
    } else
    if (movimiento == __IZ__) {
        if (columnaActiva > 0)
            columnaActiva--;
    } else
    if (movimiento == __DE__) {
        if (columnaActiva < filas - 1)
            columnaActiva++;
    } else 
    
    if (movimiento == __ENTER__){
        double valor = leerPosicion();
        matriz->set(filaActiva, columnaActiva, valor);
    }
}


double Lector::leerPosicion() {
    char valor[10];
    
    imprimir();
    
    // Placeholder del valor a modificar
    imprimirHilight("< valor >");
    
    // Posicion del area de lectura
    gotoxy(filas + 4, 5);
    cout << "Ingrese un valor para el <valor>: ";
    cin >> valor;
    
    double numero = atof(valor);
    return numero;
}





#endif


