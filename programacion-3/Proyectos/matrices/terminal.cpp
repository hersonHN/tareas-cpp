#ifndef __TERMINAL_CPP__
#define __TERMINAL_CPP__


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#include <iostream>


using namespace std;

#define ABAJO 80
#define ARRIBA 72
#define ENTER 13

void color(string text, int textColor, int BackColor) {
    
    int Color_INDEX = textColor % 15 + ((BackColor % 15) * 16);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_INDEX);
    //Coloca el color de texto deseado    
    cout << text;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 % 15 + ((15 % 15) * 16));
    // Devuelve los colores default    
}


int gotoxy(SHORT y, SHORT x){
    COORD coordenadas;
    HANDLE puntos2;
    coordenadas.Y = y;
    coordenadas.X = x;
    if ((puntos2 = GetStdHandle(STD_OUTPUT_HANDLE)) == INVALID_HANDLE_VALUE) return 0;
    if (SetConsoleCursorPosition(puntos2, coordenadas) == 0) return 0;

    return 1;
}



#endif
