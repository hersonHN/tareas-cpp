#ifndef __MAIN_CPP__
#define __MAIN_CPP__

#include <iostream>
#include <conio.h>
#include "Menu.cpp"
#include "Controlador.cpp"

using namespace std;


int main() {
        
    Menu menu = Menu(" Menu Principal ");
    
    menu.agregar(Opcion("1. Ingresar Matrices", Controlador::ingresarMatriz));
    menu.agregar(Opcion("2. Operaciones Con Matrices", Controlador::operaciones));
    menu.agregar(Opcion("3. Consulta", Controlador::consultar));
    menu.agregar(Opcion("4. Salir", Controlador::salir));
    
    menu.loop();
    
    return 0;
    
}




#endif


