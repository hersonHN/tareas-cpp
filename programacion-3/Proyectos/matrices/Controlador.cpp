#ifndef __CONTROLADOR_CPP__
#define __CONTROLADOR_CPP__

#include <iostream>
#include <string.h>
#include "Matriz/Matriz.cpp"
#include "Menu.cpp"
#include "Lector.cpp"


vector<Matriz> matrices;


class Controlador {

public:
    
    Controlador() {}
    
    
    // noop
    static int noop(int);
    
    // eventos del menu
    static int ingresarMatriz(int);
    static int operaciones(int);
    static int consultar(int);
    static int salir(int);
    
    // accioes de matrices
    
    static int matrizPorEscalar(int);
    static int matrizPorMatriz(int);
    static int calcularInversa(int);
    static int calcularDeterminante(int);
    static int verificarSimetrica(int);
    static int duplicarMatriz(int);
    
    static void esperar();
    static Matriz seleccionar(string);
    
    static void limpiar() {
        system("cls");
    }

};


int Controlador::ingresarMatriz(int opcion) {
    limpiar();
    int filas, columnas;
    
    do {
        cout << "Ingrese la cantidad de filas: (mayor que cero) ";
        cin >> filas;
    } while (filas <= 0);
    
    do {
        cout << "Ingrese la cantidad de columnas: (mayor que cero) ";
        cin >> columnas;
    } while (columnas <= 0);
    
    Matriz matriz = Matriz(filas, columnas);
    Lector lector = Lector(matriz);
    
    lector.leer();
    matrices.push_back(matriz);

    return 0;
}

int Controlador::operaciones(int opcion) {
    
    Menu menu = Menu(" Operaciones con matrices ");

    menu.agregar(Opcion("1. Multiplicar matriz por un escalar", Controlador::matrizPorEscalar));
    menu.agregar(Opcion("2. Multiplicacion entre matrices", Controlador::matrizPorMatriz));
    menu.agregar(Opcion("3. Calcular matriz inversa", Controlador::calcularInversa));
    menu.agregar(Opcion("4. Calcular determinante", Controlador::calcularDeterminante));
    menu.agregar(Opcion("5. Verificar matriz simetrica", Controlador::verificarSimetrica));
    menu.agregar(Opcion("6. Duplicar matriz", Controlador::duplicarMatriz));
    menu.agregar(Opcion("7. Salir", Controlador::salir));
    
    menu.loop();
    
    return 0;
}

int Controlador::consultar(int opcion) {

    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
    
    cout << matriz << endl;
    
    esperar();
    return 0;
}

int Controlador::noop(int opcion) {
    return 0;
}

int Controlador::salir(int opcion) {
    return -1;
}

int Controlador::matrizPorEscalar(int opcion) {
    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
        
    int numero;
    cout << "Ingrese un numero para multipicarlo por la matriz: ";
    cin >> numero;
    
    Matriz resultado = matriz * numero;
    
    cout << resultado << endl;
    esperar();
    
    return 0;
}

int Controlador::matrizPorMatriz(int opcion) {
    Matriz matriz1 = Controlador::seleccionar(" Seleccione la primer matriz ");
    Matriz matriz2 = Controlador::seleccionar(" Seleccione la segunda matriz ");
    
    Matriz resultado = matriz1 * matriz2;
    
    cout << resultado << endl;
    esperar();
    
    return 0;
}


int Controlador::calcularInversa(int opcion) {
    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
    
    Matriz inversa = matriz--;
    cout << inversa << endl;
    esperar();
    
    return 0;
}


int Controlador::calcularDeterminante(int opcion) {
    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
    
    double det = matriz++;
    cout << "Determinante: " << det << endl;
    esperar();
    
    return 0;
}

int Controlador::verificarSimetrica(int opcion) {
    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
    
    cout << matriz << endl;
    
    if (matriz.esSimetrica()) {
        cout << "Es simetrica"; 
    } else {
        cout << "No es simetrica";
    }
    cout << endl;
    
    esperar();
    return 0;
}

int Controlador::duplicarMatriz(int opcion) {
    Matriz matriz = Controlador::seleccionar(" Seleccione una matriz ");
    
    Matriz copia = matriz.duplicar();
    
    matrices.push_back(copia);
    
    cout << "Matriz duplicada!" << endl;
    cout << copia << endl;
    
    
    esperar();
    return 0;
}

Matriz Controlador::seleccionar(string titulo) {
    int cantidad = matrices.size();
        
    Menu menu = Menu(titulo);
    
    for (int x = 0; x < cantidad; x++) {
        menu.agregar(Opcion(matrices[x].serialize(), Controlador::salir));
    }
    
    menu.loop();
    
    return matrices[menu.activo];
}

void Controlador::esperar() {
    cout << endl;
    cout << "Presione cualquier tecla para regresar";
    getch();
}


#endif
