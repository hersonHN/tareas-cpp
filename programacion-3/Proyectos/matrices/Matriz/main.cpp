#ifndef __MAIN_CPP__
#define __MAIN_CPP__

#include <iostream>
#include "Matriz.cpp"

using namespace std;

int main() {
    cout << endl << endl;

    {
        cout << "SUMA DE MATRICES: " << endl;
        double valores1[][3] = {
            {  2,  0, -3 },
            {  3,  1  ,5 },
            {  1,  4,  2 }
        };

        double valores2[][3] = {
            {  1,   0, -3 },
            { -2, 0.5,  5 },
            {  3,   4,  2 }
        };

        Matriz matriz1 = Matriz(valores1);
        Matriz matriz2 = Matriz(valores2);

        Matriz resultado1 = matriz1 + matriz2;
        
        cout << matriz1 << endl;
        cout << matriz2 << endl;
        cout << resultado1 << endl;
    }


    {
        cout << "MULTIPLICACION POR UN ESCALAR: " << endl;

        double valores[][2] = {
            { 10, 20 },
            { 30, 40 },
            { 50, 60 }
        };
        
        double escalar = 2;

        Matriz matriz = Matriz(valores);
        Matriz resultado = matriz.por(escalar);
        
        cout << "Escalar: " << escalar << endl;
        cout << "Matriz: " << endl;
        cout << matriz;
        
        cout << "Resultado: " << endl;
        cout << resultado << endl;
    }


    {
        cout << "MULTIPLICACION DE MATRICES: " << endl;
        double valores1[][3] = {
            { 1, 2, 3 },
            { 4, 5, 6 }
        };

        double valores2[][2] = {
            { 10, 20 },
            { 30, 40 },
            { 50, 60 }
        };

        Matriz matriz1 = Matriz(valores1);
        Matriz matriz2 = Matriz(valores2);
        Matriz resultado1 = matriz1 * matriz2;
        
        cout << matriz1 << endl;
        cout << matriz2 << endl;  
        cout << resultado1 << endl;
    }


    {
        cout << "CALCULO DE DETERMINANTE: " << endl;
        double valoresCuadrados[][2] = {
            { 0, 4 },
            { 2, 1 }
        };
        Matriz matrizCuadrada = Matriz(valoresCuadrados);
        cout << matrizCuadrada;
        cout << "Determinante: " << matrizCuadrada.determinante() << endl;
        cout << endl;
    }


    {
        cout << "SUBMATRIZ: " << endl;
        double valoresSub[][3] = {
            { 0, 4,  5 },
            { 6, 3,  1 },
            { 2, 1, 11 }
        };
        int fila = 0;
        int columna = 0;
        Matriz matrizNotSub = Matriz(valoresSub);
        Matriz matrizSub = matrizNotSub.submatriz(fila, columna);
        cout << matrizNotSub;
        cout << matrizSub;
        cout << "[" << fila + 1 << ", " << columna + 1 << "]" << endl << endl;
    }


    {
        cout << "DETERMINANTE DE 3x3: " << endl;
        double valores[][3] = {
            { 0, 4,  5 },
            { 6, 3,  1 },
            { 2, 1, 11 }
        };
        Matriz matriz = Matriz(valores);
        cout << matriz;

        double det = matriz++;
        cout << "Determinante: " << det << endl << endl;
    }


    {
        cout << "TRASPOSICION DE UNA MATRIZ: " << endl;
        double valores[][3] = {
            { 0, 4,  5 },
            { 6, 3,  1 },
            { 2, 1, 11 }
        };
        Matriz matriz = Matriz(valores);
        Matriz traspuesta = matriz.traspuesta();
        
        cout << "Matriz: " << endl;
        cout << matriz;

        cout << "Matriz Traspuesta: " << endl;
        cout << traspuesta << endl;
    }


    {
        cout << "MATRICES SIMETRICAS:" << endl;
        double valores[][3] = {
            { 0,  4,  5 },
            { 6,  3,  1 },
            { 2,  1, 11 }
        };

        double valoresSimetricos[][3] = {
            { 0,  6,  2 },
            { 6,  3,  1 },
            { 2,  1, 11 }
        };
        Matriz matriz = Matriz(valores);
        Matriz matrizSimetrica = Matriz(valoresSimetricos);
        
        cout << matriz;
        cout << "Es simetrica? ";
        cout << (matriz.esSimetrica() ? "Si" : "No") << endl; 

        cout << matrizSimetrica;
        cout << "Es simetrica? ";
        cout << (matrizSimetrica.esSimetrica() ? "Si" : "No") << endl;
        cout << endl;
    }
    
    {
        cout << "MATRICES IGUALES:" << endl;
        double valores1[][3] = {
            { 0,  4,  5 },
            { 6,  3,  1 },
            { 2,  1, 11 }
        };

        double valores2[][3] = {
            { 0,  4,  5 },
            { 6,  3,  1 },
            { 2,  1, 11 }
        };
        Matriz matriz1 = Matriz(valores1);
        Matriz matriz2 = Matriz(valores2);
        
        cout << matriz1 << endl;
        cout << matriz2 << endl;
        cout << "Son iguales? ";
        cout << ((matriz1 == matriz2) ? "Si" : "No") << endl; 

        cout << endl;
    }
    
    {
        cout << "MATRIZ INVERSA:" << endl;
        double valores[][3] = {
            { 2,  0,  1 },
            { 3,  0,  0 },
            { 5,  1,  1 }
        };

        Matriz matriz = Matriz(valores);
        Matriz inversa = matriz.inversa();
        Matriz conprobacion = matriz * inversa;
        
        cout << matriz << endl;
        cout << inversa << endl;
        cout << conprobacion << endl;
    
        cout << endl;
    }
    
    {
        cout << "COPIAR MATRIZ:" << endl;
        double valores[][3] = {
            { 2,  0,  1 },
            { 3,  0,  0 },
            { 5,  1,  1 }
        };

        Matriz matriz = Matriz(valores);
        Matriz copia = matriz.duplicar();
        cout << matriz << endl;
        cout << copia << endl;
    
        cout << endl;
    }

    return 0;
}



#endif

