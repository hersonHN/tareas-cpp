#ifndef __MATRIZ_CPP__
#define __MATRIZ_CPP__

#include <cmath>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include "Matriz.h"
#include "operaciones.cpp"

using namespace std;

Matriz::Matriz() {
    __constructor();
}


Matriz::Matriz(int fil, int col) {
    __constructor();
    filas    = fil;
    columnas = col;

    for (int x = 0; x < fil; x++) {
        data.push_back( vector<double>() );

        for (int y = 0; y < col; y++) {
            data[x].push_back(0);
        }
    }
}


template <size_t X, size_t Y>
Matriz::Matriz(double (&arreglo)[X][Y]) {
    __constructor();
    columnas = sizeof(arreglo[0]) / sizeof(double);
    filas   = sizeof(arreglo)   / (sizeof(double) * columnas);

    for (int x = 0; x < filas; x++) {
        data.push_back( vector<double>() );

        for (int y = 0; y < columnas; y++) {
            data[x].push_back(arreglo[x][y]);
        }
    }
}

void Matriz::__constructor() {
    filas   = 0;
    columnas = 0;
    valido   = true;
    mensaje  = "";
}


int Matriz::getFilas() {
    return filas;
}


int Matriz::getColumnas() {
    return columnas;
}


double Matriz::get(int x, int y) {
    return data[x][y];
}


void Matriz::set(int x, int y, double valor) {
    data[x][y] = valor;
}


string Matriz::toString() {
    if (!esValida()) {
        return mensaje;
    }

    stringstream salida;
    double num;
    
    for (int x = 0; x < filas; x++) {
        salida << "[";

        for (int y = 0; y < columnas; y++) {
            salida << formatearNumero(x, y);
            
            if (y != columnas - 1) {
                salida << ",";
            }
        }

        salida << " ] " << endl;
    }

    return salida.str();
}

string Matriz::serialize() {
    if (!esValida()) {
        return "MATRIZ INVALIDA";
    }

    stringstream salida;
    double num;
    
    salida << "Matriz de ";
    salida << getFilas();
    salida << " x ";
    salida << getColumnas();

    return salida.str();
}

string Matriz::formatearNumero(int fil, int col) {
    double num = data[fil][col];
    char formateado[10];

    if (num == floor(num)) {
        sprintf(formateado, "%9.0f", num);
    } else {
        sprintf(formateado, "%9.2f", num);
    }
    
    string str(formateado);
    
    return str;
}


void Matriz::invalidar(string m) {
    mensaje = m;
    valido = false;
}

string Matriz::mensajeError() {
    return mensaje;
}

bool Matriz::esValida() {
    return valido;
}

bool Matriz::esCuadrada() {
    return getFilas() == getColumnas();
}


#endif
