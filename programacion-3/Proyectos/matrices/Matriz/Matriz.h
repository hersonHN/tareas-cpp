#ifndef __MATRIZ_H__
#define __MATRIZ_H__

#include <vector>
#include <string.h>

using namespace std;

class Matriz {

private:
    int filas;
    int columnas;
    bool valido;
    string mensaje;

    vector< vector<double> > data;

public:
    // Constructores
    Matriz();
    Matriz(int, int);
    template <size_t X, size_t Y> Matriz(double (&arreglo)[X][Y]);
    void __constructor();

    // Getters y Setters
    int getFilas();
    int getColumnas();
    double get(int, int);
    void set(int, int, double);
    string toString();
    string serialize();
    string formatearNumero(int, int);
    

    void invalidar(string);
    string mensajeError();
    bool esValida();
    bool esCuadrada();

    // Operaciones
    Matriz mas(Matriz);
    Matriz por(Matriz);
    Matriz por(double);
    double determinante();
    Matriz submatriz(int, int);
    Matriz traspuesta();
    Matriz inversa();
    Matriz duplicar();
    bool comparar(Matriz);
    bool esSimetrica();
    
    
    // Operadores
    Matriz operator+(Matriz m) {
        return mas(m);
    }
    
    Matriz operator*(int n) {
        return por(n);
    }
    
    Matriz operator*(Matriz m) {
        return por(m);
    }
    
    double operator++(int) {
        return determinante();
    }
    
    Matriz operator--(int) {
        return inversa();
    }
    
    bool operator==(Matriz m) {
        return comparar(m);
    }

};

ostream &operator<<(ostream &os, Matriz &m) {
    os << m.toString();
    return os;
}


#endif
