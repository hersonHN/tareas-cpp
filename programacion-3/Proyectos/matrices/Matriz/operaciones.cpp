#ifndef __OPERACIONES_CPP__
#define __OPERACIONES_CPP__

#include <cmath>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <vector>

#include "Matriz.h"
#include "Matriz.cpp"

using namespace std;

Matriz Matriz::mas(Matriz right) {
    Matriz left = *this;
    Matriz resultado;

    // si no tienen el mismo tamanio retornar invalido
    if (left.getFilas()   != right.getFilas() || 
        left.getColumnas() != right.getColumnas()) 
    {
        resultado = Matriz();
        resultado.invalidar("Las matrices no tiene las mismas dimensiones");
        return resultado;
    }

    int fil = left.getFilas();
    int col = left.getColumnas();
    double valor;

    resultado = Matriz(fil, col);

    for (int x = 0; x < fil; x++) {
        for (int y = 0; y < col; y++) {
            valor = left.get(x, y) + right.get(x, y);
            resultado.set(x, y, valor);
        }
    } 

    return resultado;
}


// Matriz x Escalar
Matriz Matriz::por(double escalar) {

    int filas = getFilas();
    int columnas = getColumnas();
    Matriz resultado = Matriz(filas, columnas);
    
    double valor;

    for (int f = 0; f < filas; f++) {
        for (int c = 0; c < columnas; c++) {
            valor = get(f, c) * escalar;
            resultado.set(f, c, valor);
        }
    }

    return resultado;
}


// Matriz x Matriz
Matriz Matriz::por(Matriz right) {
    Matriz left = *this;
    Matriz resultado;

    if (left.getColumnas() != right.getFilas()) {
        resultado.invalidar("Las columnas y filas "
            "de las matrices no concuerdan (la matriz "
            "del lado izquierdo debe tener la misma "
            "cantidad de filas que las columnas de la "
            "matriz del lado derecho)");
        return resultado;
    }

    int resultanteFil = left.getFilas();
    int resultanteCol = right.getColumnas();
    int sumasRealizables = left.getColumnas();

    resultado = Matriz(resultanteFil, resultanteCol);
    double valor;

    // Multiplicamos las filas de la primer matriz por las columnas de la segunda

    for (int fil = 0; fil < resultanteFil; fil++) {
        for (int col = 0; col < resultanteCol; col++) {

            valor = 0;
            for (int sumas = 0; sumas < sumasRealizables; sumas++) {
                valor += left.get(fil, sumas) * right.get(sumas, col);
            }
            resultado.set(fil, col, valor);

        }
    }


    return resultado;
}


double Matriz::determinante() {
    if (!esCuadrada()) return 0;
    
    int cols = getColumnas();

    if (cols == 2) {
        return  (get(0, 0) * get(1, 1)) -
                (get(1, 0) * get(0, 1));
    }
    
    int det = 0;
    int detAcum = 0;
    int exp = -1;
    int valorCelda;
    Matriz subm;

    if (cols == 1) {
        return get(0, 0);
    }

    for (int n = 0; n < cols; n++) {

        exp *= -1; // Empieza con 1, luego -1, 1, -1, etc...
        subm = submatriz(0, n);
        valorCelda = get(0, n);

        det = subm.determinante();
        detAcum = detAcum + (det * exp * valorCelda);
    }

    return detAcum;
}


Matriz Matriz::submatriz(int fil, int col) {
    Matriz resultado;
    if (!esCuadrada()) {
        resultado.invalidar("La matriz no es cuadrada");
        return resultado;
    }

    int filas = getFilas();
    int columnas = getColumnas();
    int filaActual = 0;
    int columnaActual = 0;
    double valor;

    resultado = Matriz(filas - 1, columnas - 1);

    for (int f = 0; f < filas; f++) {

        columnaActual = 0;
        for (int c = 0; c < columnas; c++) {
        
            if ( !(f == fil || c == col) ) {
                valor = get(f, c);
                resultado.set(filaActual, columnaActual, valor);
                columnaActual++;
            }
        }
        if (f != fil) filaActual++;
    }

    return resultado;
}


Matriz Matriz::traspuesta() {
    Matriz resultado;
    if (!esCuadrada()) {
        resultado.invalidar("La matriz debe ser "
            "cuadra para poder calcular su matriz traspuesta");
        return resultado;
    }

    int espacios = getColumnas();
    resultado = Matriz(espacios, espacios);
    
    double valor;

    for (int fil = 0; fil < espacios; fil++) {
        for (int col = 0; col < espacios; col++) {
            valor = get(fil, col);
            resultado.set(col, fil, valor);
        }
    }

    return resultado;
}


bool Matriz::comparar(Matriz otra) {
        int estasFilas, estasColumnas, 
            esasFilas, esasColumnas;

        estasFilas    = getFilas();
        estasColumnas = getColumnas();
        
        esasFilas    = otra.getFilas();
        esasColumnas = otra.getColumnas();

        if (estasFilas != esasFilas || estasColumnas != esasColumnas) {
            return false;
        }

        double esteValor, eseValor;

        for (int fil = 0; fil < estasFilas; fil++) {
            for (int col = 0; col < estasColumnas; col++) {

                esteValor = get(fil, col);
                eseValor  = otra.get(fil, col);

                if (esteValor != eseValor) {
                    return false;
                }
            }
        }

        return true;
}

bool Matriz::esSimetrica() {
    if (!esCuadrada()) return false;
    return comparar(traspuesta());
} 


Matriz Matriz::inversa() {
    Matriz resultado;
    if (!esCuadrada()) {
        resultado = Matriz();
        resultado.invalidar("La matriz debe ser cuadrada para calcular su inversa");
        return resultado; 
    }
    
    double determinante_ = abs(determinante());
    
    if (determinante_ == 0) {
        resultado = Matriz();
        resultado.invalidar("La matriz no tiene inversa");
        return resultado;
    }
    
    int fil = getFilas();
    int col = getColumnas();
    
    Matriz adjunta = Matriz(fil, col);
    
    Matriz subm;
    double det;
    int multiplicador = 0;
    
    for (int f = 0; f < fil; f++) {
        for (int c = 0; c < col; c++) {
            subm = submatriz(f, c);
            det  = subm.determinante();
            
            multiplicador = (f % 2 == c % 2) ? 1 : -1;
            det = det * multiplicador;
            
            adjunta.set(f, c, det / determinante_);
        }
    }
    
    resultado = adjunta.traspuesta();
    
    return  resultado;
}


Matriz Matriz::duplicar() {
    Matriz resultado;

    int fil = getFilas();
    int col = getColumnas();
    
    Matriz copia = Matriz(fil, col);    
    
    for (int f = 0; f < fil; f++) {
        for (int c = 0; c < col; c++) {
            copia.set(f, c, get(f, c));
        }
    }
    
    return copia;
}


#endif
