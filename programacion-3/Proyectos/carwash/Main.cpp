
#ifndef __MAIN_CPP__
#define __MAIN_CPP__

#include <iostream>
#include <ctime>
#include <string.h>
#include <conio.h>
#include "Carwash.cpp"
#include "general.cpp"
#include "terminal.cpp"
#include "menu.cpp"

using namespace std;

int configuracion    (int);
int ingresar_vehiculo(int);
int sacar_vehiculo   (int);
int simular          (int);

void limpiar();

bool accion_random();

bool agregar_carro_a_la_fila(int);
bool lavar_carro();
bool sacar_carro_de_la_fila_principal();
bool sacar_carro_de_la_fila_secundaria();

void sacar_carro(Fila&, int);
Fila carros_random(int, int);

int noop (int foo) { return 0; }
int salir (int foo) { return -1; }

Carwash carwash = Carwash();
Menu menu = Menu("Opciones");

int main() {

    // Seed para los numeros random
    srand((unsigned)time(NULL));

    carwash.posicionar(15, 62);

    menu.noLimpiar();
    menu.posicion(15);
    menu.agregar(Opcion("Configuracion",        configuracion));
    menu.agregar(Opcion("Ingreso de Vehiculos", ingresar_vehiculo));
    menu.agregar(Opcion("Salida de Vehiculos",  sacar_vehiculo));
    menu.agregar(Opcion("Simulacion",           simular));

    carwash.tiempo = 1;
    carwash.filas(5);
    int cant = carwash.tamanio_filas;
    carwash.fila_principal  = carros_random(cant, 1);
    carwash.fila_secundaria = carros_random(cant, 4);

    carwash.dibujar();

    menu.loop();
    limpiar();
    return 0;

}

int configuracion(int opcion) {
    limpiar();

    int tiempo = pedir_numero("Ingrese tiempo del area de lavado (en segundos): ");
    int tamanio_fila = pedir_numero("Ingrese el tamanio de las filas (< 6 y > 0): ");

    if (tamanio_fila > 5) tamanio_fila = tamanio_fila % 6;
    if (tamanio_fila < 1) tamanio_fila = 2;

    carwash.tiempo = tiempo;
    carwash.filas(tamanio_fila);

    limpiar();
    carwash.dibujar();
    menu.loop();

}

void limpiar() {
    system("cls");
}

int ingresar_vehiculo(int opcion) {
    limpiar();

    if (carwash.fila_espera.size() == carwash.tamanio_filas) {
        cout << "La fila esta!!! presione cualquier tecla para seguir.";
        getch();
        return 0;
    }

    int placa = pedir_numero("Ingrese el numero de placa: ");

    limpiar();
    carwash.dibujar();

    if (carwash.fila_espera.size() == 0) {
        agregar_carro_a_la_fila(placa);
    }
    lavar_carro();

    return 0;
}

int sacar_vehiculo (int opcion) {
    if ((carwash.fila_principal.size() == 0) && (carwash.fila_secundaria.size() == 0))
       return 0;
    limpiar();

    Menu carros = Menu("Listado de carros segun su numero de placa:");

    int placa;
    // Agregamos los carros de la fila principal
    for (int n = 0, limite = carwash.fila_principal.size(); n < limite; n++) {
        placa = carwash.fila_principal[n].placa;
        carros.agregar(Opcion(to_string(placa), salir));
    }

    // Agregamos los carros de la fila secundaria
    for (int n = 0, limite = carwash.fila_secundaria.size(); n < limite; n++) {
        placa = carwash.fila_secundaria[n].placa;
        carros.agregar(Opcion(to_string(placa), salir));
    }

    carros.loop();
    limpiar();
    carwash.dibujar();

    int indice = carros.activo;
    if (indice == -1) return 0;

    if (indice >= carwash.fila_principal.size()) {
        indice = indice - carwash.fila_principal.size();
        sacar_carro(carwash.fila_secundaria, indice);
    } else {
        sacar_carro(carwash.fila_principal, indice);
    }

}

int simular(int opcion) {

    int cant = carwash.tamanio_filas;

    carwash.dibujarLineas();

    int x = 30;
    while (x--) {
        if (!accion_random()) x++;
        else Sleep(300);
    }
    return 0;
}

Fila carros_random(int max, int linea) {
    int cant = rand() % max;

    Fila fila = Fila();
    Carro carro;

    for (int i = 0; i < cant; i++) {
        carro.x = linea;
        carro.y = Carro::tamanio * i;
        carro.placa_random();
        fila.push_back(carro);
    }

    return fila;
}

bool accion_random() {
    int indice = rand() % 4;

    if (!carwash.parqueo_lleno() && carwash.fila_espera.size() > 0) {
        return lavar_carro();
    }

    if (indice == 0) return agregar_carro_a_la_fila(-1);
    if (indice == 2) return sacar_carro_de_la_fila_principal();
    if (indice == 3) return sacar_carro_de_la_fila_secundaria();

    return false;
}

bool agregar_carro_a_la_fila(int placa) {
    if (carwash.fila_espera.size() == carwash.tamanio_filas) {
        return false;
    }

    int tamanio = carwash.fila_espera.size();
    Carro carro;

    int pos;
    for (int i = tamanio; i--> 0;) {
        pos = (i + 1) * Carro::tamanio;
        carwash.fila_espera[i].mover(10, pos);
    }

    carro = Carro();
    carro.x = 10;
    carro.y = 0;
    if (placa == -1) {
        carro.placa_random();
    } else {
        carro.placa = placa;
    }
    carwash.fila_espera.push_front(carro);
    carro.dibujar();

    return true;
}

bool lavar_carro() {
    if (carwash.fila_espera.size() == 0) return false;

    Carro carro = carwash.fila_espera.back();
    carwash.fila_espera.pop_back();

    carro.mover(20, 50);
    carro.mover(20, 65);

    Sleep(carwash.tiempo * 1000);

    carro.mover(20, 50);
    carro.mover(16, 50);
    carro.mover(16, 65);

    // Escoger a que fila de parqueo ira
    // Si no hay ninguna libre, nos vamos

    if (carwash.parqueo_lleno()) {
        carro.mover(16, 80 - Carro::tamanio);
        carro.limpiar();
        return true;
    }

    int pos_x;
    Fila *fila;
    if (carwash.fila_principal.size() < carwash.tamanio_filas) {
        pos_x = 1;
        fila = &carwash.fila_principal;
    } else {
        pos_x = 4;
        fila = &carwash.fila_secundaria;
    }

    carro.mover(pos_x, 65);
    carro.mover(pos_x, fila->size() * Carro::tamanio);
    fila->push_back(carro);


    return true;
}

bool sacar_carro_de_la_fila_principal() {
    if (carwash.fila_principal.size() == 0) return false;

    int tamanio = carwash.fila_principal.size();
    int indice_carro = rand() % tamanio;
    sacar_carro(carwash.fila_principal, indice_carro);

    return true;
}

bool sacar_carro_de_la_fila_secundaria() {
    if (carwash.fila_secundaria.size() == 0) return false;

    int tamanio = carwash.fila_secundaria.size();
    int indice_carro = rand() % tamanio;
    sacar_carro(carwash.fila_secundaria, indice_carro);

    return true;
}

void sacar_carro(Fila& fila, int index) {

    if (fila.size() == 0) return;

    Carro carro;
    int ultimo = fila.size() - 1;
    int linea_x = fila[index].x;

    // Sacando los carros que estan delante del nuestro y pasandolos a
    // la linea temporal
    while (ultimo > index) {
        carro = fila[ultimo];
        carro.mover(7, 60);
        carro.mover(7, carwash.fila_temporal.size() * Carro::tamanio);
        carwash.fila_temporal.push_back(carro);
        fila.pop_back();

        ultimo--;
    }


    // Sacamos nuestro carro
    carro = fila[index];
    carro.mover(carro.x, 80 - Carro::tamanio);
    carro.limpiar();
    fila.pop_back();


    // Volvemos a colocar los autos que movimos
    ultimo = carwash.fila_temporal.size() - 1;

    while (carwash.fila_temporal.size()) {
        carro = carwash.fila_temporal[ultimo];

        carro.mover(7, 60);
        carro.mover(linea_x, 60);
        carro.mover(linea_x, fila.size() * Carro::tamanio);

        fila.push_back(carro);
        carwash.fila_temporal.pop_back();

        ultimo--;
    }


}




#endif

