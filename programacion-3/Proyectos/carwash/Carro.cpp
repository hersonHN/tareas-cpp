
#include "Main.cpp"

#ifndef __CARRO_CPP__
#define __CARRO_CPP__

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "terminal.cpp"

using namespace std;


class Carro {
    public:
    int x;
    int y;
    int placa;
    int color_carro;

    int direccion_x;
    int direccion_y;
    int tipo;
    static const int tamanio = 12;

    string derecha[2];
    string izquierda[2];
    string espacios;


    Carro () {
        x = y = 0;
        direccion_x = direccion_y = 1;

        tipo = (rand() % 10 != 7) ? 1 : 2;
        color_carro = 8 + (rand() % 6);

        if (tipo == 1) {
            derecha[0]   = " _/  \\___ ";
            derecha[1]   = "'-o----o-'";

            izquierda[0] = " ___/  \\_ ";
            izquierda[1] = "'-o----o-'";
            espacios     = "          ";

        } else

        if (tipo == 2) {
            derecha[0]   = " _[__]=== ";
            derecha[1]   = "(______)  ";

            izquierda[0] = " ===[__]_ ";
            izquierda[1] = "  (______)";
            espacios     = "          ";
        }
    }

    void dibujar() {
        string linea1, linea2;

        gotoxy(x, y);
        if (direccion_y == 1) {
            linea1 = derecha[0];
            linea2 = derecha[1];
        } else {
            linea1 = izquierda[0];
            linea2 = izquierda[1];

        }

        gotoxy(x, y);
        color(linea1, color_carro, 0);

        gotoxy(x + 1, y);
        color(linea2, color_carro, 0);
    }

    void limpiar() {
        gotoxy(x, y);
        cout << espacios;

        gotoxy(x + 1, y);
        cout << espacios;
    }

    Carro mover(int fil, int col) {
        direccion_x = (fil >= x) ? 1 : -1;
        direccion_y = (col >= y) ? 1 : -1;

        while (col != y) {
            Sleep(10);
            limpiar();
            y += direccion_y;
            dibujar();
        }

        while (fil != x) {
            Sleep(20);
            limpiar();
            x += direccion_x;
            dibujar();
        }

        return *this;
    }

    void placa_random() {
        placa = (rand() % 9000) + 1000;
    }
};



#endif



