#ifndef __MENU_CPP__
#define __MENU_CPP__


#include <iostream>
#include <vector>
#include <string.h>
#include "terminal.cpp"
#include "general.cpp"

using namespace std;


class Opcion {
    public:
    string nombre;
    Callback accion;

    Opcion(string n, Callback a) {
        nombre = n;
        accion = a;
    }
};

class Menu {
private:
    string titulo;
    bool limpiar;

    int filaIncial;

    void imprimirMenu();
    void imprimirEntrada(int);
    bool procesarAccion(int);
    void limpiarPantalla();

public:
    int activo;
    vector<Opcion> opciones;

    void loop();
    void posicion(int);

    Menu(string t) {
        titulo = t;
        activo = 0;
        filaIncial = 0;
    }

    void noLimpiar() {
        limpiar = false;
    }

    Menu agregar(Opcion opt) {
        opciones.push_back(opt);

        return *this;
    }
};



void Menu::loop() {

    while(1) {
        imprimirMenu();

        int accion = getch();
        bool estado = procesarAccion(accion);
        if (!estado) break;
    }

}

void Menu::limpiarPantalla() {
    if (limpiar == true) {
        system("cls");
    }
    gotoxy(filaIncial, 0);
}


void Menu::posicion(int fila) {
    filaIncial = fila;
}


void Menu::imprimirMenu() {
    limpiarPantalla();

    cout << endl;
    cout << titulo << endl;
    cout << string(30, '=') << endl;


    for (int n = 0; n < opciones.size(); n++) {
        imprimirEntrada(n);
    }
}

void Menu::imprimirEntrada(int opcion) {
    if (opcion == activo) {
        color(opciones[opcion].nombre, 14, 0);
    } else {
        cout << opciones[opcion].nombre;
    }

    cout << endl;
}

bool Menu::procesarAccion(int accion) {
    if (accion == __ENTER__) {
        int resultado = opciones[activo].accion(activo);
        if (resultado == -1) {
            limpiarPantalla();
            return false;
        }
    }
    else
    if (accion == __ESC__) {
        limpiarPantalla();
        return false;
    }
    else
    if (accion == __AB__) {
        if (activo < opciones.size() - 1) activo++;
    }
    else
    if (accion == __AR__) {
        if (activo != 0) activo--;
    }

    return true;
}






#endif

