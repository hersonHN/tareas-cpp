#ifndef __GENERAL_CPP__
#define __GENERAL_CPP__

#include <deque>
#include <string.h>
#include "Carro.cpp"


typedef int (*Callback) (int a);
typedef deque<Carro> Fila;

const int __ENTER__ = 13;
const int __ESC__ = 27;
const int __AB__ = 80;
const int __AR__ = 72;
const int __IZ__ = 75;
const int __DE__ = 77;


string to_string(int number){
    string number_string = "";
    char ones_char;
    int ones = 0;
    while(true){
        ones = number % 10;
        switch(ones){
            case 0: ones_char = '0'; break;
            case 1: ones_char = '1'; break;
            case 2: ones_char = '2'; break;
            case 3: ones_char = '3'; break;
            case 4: ones_char = '4'; break;
            case 5: ones_char = '5'; break;
            case 6: ones_char = '6'; break;
            case 7: ones_char = '7'; break;
            case 8: ones_char = '8'; break;
            case 9: ones_char = '9'; break;
        }
        number -= ones;
        number_string = ones_char + number_string;
        if(number == 0){
            break;
        }
        number = number/10;
    }
    return number_string;
}

#endif



