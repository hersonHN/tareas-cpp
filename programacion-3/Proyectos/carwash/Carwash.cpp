
#include "Main.cpp"

#ifndef __CARWASH_CPP__
#define __CARWASH_CPP__

#include <deque>
#include "Carro.cpp"
#include "general.cpp"
#include "terminal.cpp"

using namespace std;

class Carwash {
    public:
    int tiempo;
    int tamanio_filas;

    int x;
    int y;

    Fila fila_principal;
    Fila fila_secundaria;
    Fila fila_temporal;
    Fila fila_espera;

    Carwash() {
        fila_principal  = Fila();
        fila_secundaria = Fila();
        fila_temporal   = Fila();
        fila_espera     = Fila();
    }

    void posicionar(int pos_x, int pos_y) {
        x = pos_x;
        y = pos_y;
    }

    void filas(int tamanio) {
        tamanio_filas = tamanio;
    }

    void dibujar() {
        // carwash
        int tamanio = Carro::tamanio;


        coutin("mmmm CARWASH mmmm", x + 3, y);
        coutin("m               m", x + 4, y);
        coutin("                m", x + 5, y);
        coutin("                m", x + 6, y);
        coutin("m               m", x + 7, y);
        coutin("mmmmmmmmmmmmmmmmm", x + 8, y);

        // parqueos
        coutin(string(tamanio_filas * tamanio, '='), 0, 0);
        coutin(string(tamanio_filas * tamanio, '='), 3, 0);
        coutin(string(tamanio_filas * tamanio, '='), 6, 0);
        coutin(string(tamanio_filas * tamanio, '='), 9, 0);

        dibujarLineas();
    }

    void dibujarLineas() {

        imprimirLinea(fila_principal, 1);
        imprimirLinea(fila_secundaria, 4);
        imprimirLinea(fila_temporal, 7);
        imprimirLinea(fila_espera, 10);
    }

    void imprimirLinea(Fila linea, int pos) {
        Carro carro;
        for (int i = 0, l = linea.size(); i < l; i++) {
            carro = linea[i];
            carro.x = pos;
            carro.y = Carro::tamanio * i;
            carro.dibujar();
        }
    }

    void coutin(string texto, int fil, int col) {
        gotoxy(fil, col);
        cout << texto;
    }

    bool parqueo_lleno() {
        return (fila_principal.size() == tamanio_filas &&
               fila_secundaria.size() == tamanio_filas);
    }
};


#endif



