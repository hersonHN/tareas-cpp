/*
    2. Sobrecarga Operadores:
    Crear una clase llamada NumeroRacional con lo siguiente:

    a. Constructor que simplifique la fraccion y que evite denominador 0
    b. Sobrecargar la suma, resta, multiplicacion y division.
    c. Sobrecargar los operadores relacionales y de igualdad
    d. Sobrecargar los operadores de lectura y escritura de datos
*/

#include <iostream>
#include <string.h>
#include <sstream>
#include <conio.h>

using namespace std;

class Fraccion {

public:
    int numerador;
    int denominador;
   
    Fraccion (int n, int d) {
        if (d == 0) {
            cout << "El denominador no puede ser 0" << endl;
            return;
        }
        numerador = n;
        denominador = d;
    }
    
    int MCD(int n, int d) {
        int menor = (n < d) ? n : d;
        int mayor = (n < d) ? d : n;
        int residuo = menor;
        int resultado = menor;

        while (true){
            resultado = residuo;
            residuo = mayor % menor;
            if (residuo == 0){
                break;
            }
            mayor = menor;
            menor = residuo;
        }
        
        if(resultado > 0){
            return resultado;
        } else {
            return 1;
        }
    }
    
    void simplificar() {
        int mcd = MCD(numerador, denominador);
        numerador = numerador / mcd;
        denominador = denominador / mcd;
    }
    
    string toString() {
        stringstream salida;
        salida << numerador << "/" << denominador;
        return salida.str();
    }
    
    Fraccion sumar(Fraccion uno, Fraccion dos) {
        int deno = uno.denominador * dos.denominador;
        int nume1 = (deno / uno.denominador * uno.numerador);
        int nume2 = (deno / dos.denominador * dos.numerador);
        Fraccion resultado = Fraccion(nume1 + nume2, deno);
        resultado.simplificar();
        
        return resultado;
    }
    
    Fraccion multiplicar(Fraccion uno, Fraccion dos) {
        Fraccion resultado = Fraccion(uno.numerador   * dos.numerador,
                                      uno.denominador * dos.denominador);
        resultado.simplificar();
        return resultado;
    }
    
    Fraccion operator+(Fraccion dos) {
        return dos.sumar(dos, *this);
    }
    
    Fraccion operator-(Fraccion uno) {
        Fraccion dos = Fraccion(numerador, denominador);
        dos.numerador = dos.numerador * -1;
        return sumar(uno, dos);
    }
    
    Fraccion operator*(Fraccion uno) {
        return multiplicar(uno, *this);
    }

    Fraccion operator/(Fraccion divisor) {
        divisor = Fraccion(divisor.denominador, divisor.numerador);
        return multiplicar(*this, divisor);
    }

    friend ostream& operator<<(ostream& os, Fraccion m) {
        os << m.toString();
        return os;
    }
};



int main() {
    Fraccion miFraccion = Fraccion(54, 81);
    cout << miFraccion << endl;
    miFraccion.simplificar();
    cout << miFraccion << endl;
    
    Fraccion unTercio = Fraccion(1, 3);
    Fraccion unCuarto = Fraccion(1, 4);
    
    cout << unCuarto << " + " << unTercio << " = " << (unCuarto + unTercio) << endl;
    cout << unCuarto << " - " << unTercio << " = " << (unCuarto - unTercio) << endl;
    cout << unCuarto << " * " << unTercio << " = " << (unCuarto * unTercio) << endl;
    cout << unCuarto << " / " << unTercio << " = " << (unCuarto / unTercio) << endl;
    getch();
}
