#include <iostream>
#include <conio.h>

using namespace std;
const int diasmes[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

class Fecha {

private:
    int dia, mes, anio;
   
    
public:
    Fecha(int d, int m, int a) {
        dia = d;
        mes = m;
        anio = a;
    }

    friend ostream &operator<<( ostream &o, Fecha &f ) {
        cout << f.dia << "/" << f.mes << "/" << f.anio;
    }
    
    friend istream &operator>>( istream &is, Fecha &f ) {
        cout << "Ingrese dia/enter, mes/enter, anio/enter" << endl;
        is >> f.dia >> f.mes >> f.anio;
        return is;
    }

    bool esBisiesto() {
        if (anio % 400 == 0) return true;
        if ((anio % 4 == 0) && (anio % 100 != 0)) return true;
        
        return false;
    }
    
    bool esFinDeMes() {
        
        if (mes == 2 && esBisiesto()) {
            int ultimo = (esBisiesto() ? 29 : 28);
            return dia == ultimo;
        }
        return (diasmes[mes - 1] == dia);
    }
    
    void incrementar() {
        if (!esFinDeMes()) {
            dia++;
            return;
        }
        if (mes != 12) {
            dia = 1;
            mes++;
        }
        
        dia = 1;
        mes = 1;
        anio++;
    }
    
    Fecha &operator++() {
        incrementar();
        return *this;
    }

    Fecha &operator++(int) {
        Fecha temp = *this;
        incrementar();
        return temp;
    }
    
    bool operator>(Fecha fech) {
        return ((anio * 100 * 100) + (mes * 100) + dia >
                (fech.anio * 100 * 100) + (fech.mes * 100) + fech.dia);
    }

    Fecha operator+=(int cantidad) {
        while(cantidad-- > 0)
            incrementar();
    }
};

int main () {
    Fecha f1 = Fecha(29, 2, 2012);

    //cin >> f1;
    cout << f1 << endl;
    f1 += 9;
    cout << f1 << endl;
    
    cout << (Fecha(12, 2, 2001) > Fecha(13, 2, 2001)) << endl;
    cout << (Fecha(12, 2, 2001) > Fecha(11, 2, 2001)) << endl;
    getch();
}
