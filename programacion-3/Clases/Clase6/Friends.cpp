#include <iostream>
#include <conio.h>

using namespace std;

class Numero {

private:
    int num;

public:
    Numero() {
        num = 0;
    }
    friend void modificar(Numero &n) {
        n.num = 5;
    }

    void mostrar() {
        cout << num << endl;
    }
};

int main () {
    Numero test = Numero();
    modificar(test);
    test.mostrar();
    
    getch();
}
