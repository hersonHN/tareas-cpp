#include <iostream>
#include <conio.h>

using namespace std;

void cuadrificar(int *puntero) {
    *puntero = (*puntero) * (*puntero);
}

int main () {

    int numero = 3;
    int *puntero = &numero;
    
    cout << *puntero << endl;
    cuadrificar(puntero);
    cout << *puntero << endl;

    getch();
}


