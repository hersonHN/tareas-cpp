#include <iostream>
#include <conio.h>

using namespace std;

void modificar(int &puntero) {
    puntero = 8;
}

int main () {

    int miNumero = 5;
    int *miPuntero = &miNumero;

    cout << miNumero << endl;
    cout << &miNumero << endl << endl;


    cout << miPuntero << endl;
    cout << *miPuntero << endl;
    cout << &miPuntero << endl << endl;

    modificar(miNumero);
    cout << miNumero << endl << endl;


    int numeros[] = {2, 4, 8, 16};
    int *punteroDelArreglo = numeros;

    cout << punteroDelArreglo[0] << endl;
    punteroDelArreglo++;

    cout << punteroDelArreglo[0] << endl;
    punteroDelArreglo += 3;

    cout << punteroDelArreglo[0] << endl;

    getch();
}


