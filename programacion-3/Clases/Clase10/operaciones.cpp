#include <iostream>
#include <conio.h>

using namespace std;

template <class T>
T sumar(T &a, T &b) {
    return a + b;
}

template <class T>
class prueba {
public:
    T valor;
    prueba <T> () {
        cout << "creado" << endl;
    }
};


int main () {
    int a = 2, b = 3;
    cout << sumar(a, b) << endl;

    float aa = 0.1;
    float bb = 0.2;
    cout << sumar(aa, bb) << endl;

    prueba <int> hola = prueba<int>();
    getch();
}
