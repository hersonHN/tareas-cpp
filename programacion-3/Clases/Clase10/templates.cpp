#include <iostream>
#include <conio.h>

using namespace std;

template <class T>
void intercambiar(T &a, T &b) {
    T temp;
    temp = a;
    a = b;
    b = temp;
}

int main () {
    float mi_variable = 3;
    float tu_variable = 5;
    intercambiar(mi_variable, tu_variable);
    cout << "Mi variable: " << mi_variable << endl;
    cout << "Tu variable: " << tu_variable << endl;

    getch();
}
