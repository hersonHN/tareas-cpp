#include <iostream>
#include <map>
#include <string>
#include <ctype.h>
#include <conio.h>

using namespace std;

int main() {
    map<char, string> dias = map<char, string>();
    dias['D'] = "Domingo";
    dias['L'] = "Lunes";
    dias['M'] = "Martes";
    dias['X'] = "Miercoles";
    dias['J'] = "Jueves";
    dias['V'] = "Viernes";
    dias['S'] = "Sabado";

    char indice;
    map<char, string>::iterator i;

    do {
        cout << "�Que dia desea mostrar? (D, L, M, X, J, V, S) ";
        cin >> indice;
        indice = toupper(indice);
        i = dias.find(indice);
    } while(i == dias.end());

    cout << dias[indice];

    getch();
}

