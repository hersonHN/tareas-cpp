#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <conio.h>

using namespace std;

int main() {

    queue<int> cola = queue<int>();
    set<int> grupo = set<int>();
    stack<int> pila = stack<int>();

    cola.push(3);
    cola.push(1);
    cola.push(4);
    cola.push(1);
    cola.push(6);

    cout << "Cola: ";

    int elemento;

    while( !cola.empty() ) {
        elemento = cola.front();
        cout << elemento;

        grupo.insert(elemento);
        cola.pop();
    }

    cout << endl;
    cout << "Set: ";

    set<int>::iterator it;
    for (it = grupo.begin(); it != grupo.end(); ++it) {
        elemento = *it;
        cout << elemento;
        pila.push(elemento);
    }

    cout << endl;
    cout << "Pila: ";

    while (!pila.empty()) {
        cout << pila.top();
        pila.pop();
    }
    getch();
}

