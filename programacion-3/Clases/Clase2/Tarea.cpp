#include <iostream>
#include <string.h>
#include <conio.h>

using namespace std;

int main() {
    string nombre;
    char zona;
    float ventas, isv, subtotal, total;
    
    cout << "Nombre: "; cin >> nombre;
    cout << "Ventas: "; cin >> ventas;
    cout << "Zona:   "; cin >> zona;
    
    if (zona == 'a') {
        isv = ventas * 0.10;
    }
    if (zona == 'b') {
        isv = ventas * 0.20;
    }
    if (zona == 'c') {
        isv = ventas * 0.15;
    }

    subtotal = isv + ventas;
    
    cout << nombre << ", vendio " << subtotal;
    
    getch();
}
