#include <iostream>
#include <string.h>
#include <conio.h>
#include <iomanip>

using namespace std;

int main() {

    int size = 5;
    int arreglo[] = {31324, 214521, 21, 4244, 31414};
    

    cout << "Elemento" << setw(13) << "Valor" << endl;

    for (int i = 0; i < size; i++) {
        cout  << setw(7) << i << setw(13) << arreglo[i] << endl;
    }

    getch();
}
