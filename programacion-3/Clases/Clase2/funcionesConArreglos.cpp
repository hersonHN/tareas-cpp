#include <iostream>
#include <string.h>
#include <conio.h>
#include <iomanip>

using namespace std;

void imprimir(int[], int);
void modificar(int[], int);

int main() {

    int size = 5;
    int arreglo[] = {31324, 214521, 21, 4244, 31414};

    imprimir(arreglo, size);
    modificar(arreglo, size);
    imprimir(arreglo, size);

    getch();
}


void modificar(int arreglo[], int size) {
    for (int i = 0; i < size; i++) {
        arreglo[i] = 2;
    }
}

void imprimir(int arreglo[], int size) {
    cout << "Elemento" << setw(13) << "Valor" << endl;
    
    for (int i = 0; i < size; i++) {
        cout  << setw(7) << i << setw(13) << arreglo[i] << endl;
    }
}
