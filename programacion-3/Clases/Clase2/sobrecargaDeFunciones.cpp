#include <iostream>
#include <string.h>
#include <conio.h>
#include <iomanip>

using namespace std;

// Declaracion de procedimiento
int suma(int, int);
double suma(double, double);
int suma(int, int, int);


int main() {
    cout << suma(35, 7) << endl;
    cout << suma(0.1, 0.2) << endl;
    cout << suma(35, 7, 1000) << endl;

    getch();
}

// Asignar funcion al procedimiento
int suma(int numero1, int numero2) {
    return numero1 + numero2;
}

double suma(double numero1, double numero2) {
    return numero1 + numero2;
}

int suma(int numero1, int numero2, int numero3) {
    return numero1 + numero2 + numero3;
}

