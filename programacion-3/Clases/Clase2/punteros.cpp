#include <iostream>
#include <string.h>
#include <conio.h>
#include <iomanip>

using namespace std;

// Declaracion de procedimiento
void porValor(int);
void porReferencia(int&);


int main() {
    int x = 5;

    cout << x << endl;
    
    porValor(x);      cout << "Por Valor: "      << x << endl;
    porReferencia(x); cout << "Por Referencia: " << x << endl;

    getch();
}


void porValor(int x) {
    x = 8;
    cout << "valor x: " << x << endl;
}

void porReferencia(int &x) {
    x = 9;
    cout << "valor x: " << x << endl;
}



