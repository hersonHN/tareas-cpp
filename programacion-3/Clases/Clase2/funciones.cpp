#include <iostream>
#include <string.h>
#include <conio.h>
#include <iomanip>

using namespace std;

// Declaracion de procedimiento
int suma(int, int);


int main() {
    cout << suma(35, 7);
    
    getch();
}

// Asignar funcion al procedimiento
int suma(int numero1, int numero2) {
    return numero1 + numero2;
}



