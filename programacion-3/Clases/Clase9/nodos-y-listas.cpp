#include <iostream>
#include <conio.h>
#include <stdio.h>

using namespace std;

class Nodo {
public:
    int dato;
    Nodo *siguiente;

    Nodo (int n) {
        dato = n;
        siguiente = 0;
    }
};


class Lista {
public:
    Nodo *primero;
    Nodo *ultimo;

    Lista() {
        primero = ultimo = 0;
    }

    ~Lista() {
        while (!isEmpty()) removeFront();
    }

    bool isEmpty() {
        return primero == 0;
    }

    void pushFront(int val) {
        Nodo *newPoint = new Nodo(val);

        if (isEmpty()) {
            primero = ultimo = newPoint;
        } else {
            newPoint->siguiente = primero;
            primero = newPoint;
        }
    }

    void pushBack(int val) {
        Nodo *newPoint = new Nodo(val);

        if (isEmpty()) {
            ultimo = primero = newPoint;
        } else {
            ultimo->siguiente = newPoint;
            ultimo = newPoint;
        }
    }

    bool removeFront() {
        Nodo *temp = primero;
        if (primero->siguiente == 0) ultimo = 0;
        primero = primero->siguiente;
        delete temp;
        return !isEmpty();
    }

    bool removeBack() { // XXX
        Nodo *temp = primero;
        if (primero->siguiente == 0) ultimo = 0;
        primero = primero->siguiente;
        delete temp;
        return !isEmpty();
    }

    void print() {
        Nodo *actual;
        actual = primero;
        if (actual == 0) {
            cout << "[-]" << endl;
            return;
        }

        cout << actual->dato << "->";

        while(actual->siguiente != 0) {
            actual = actual->siguiente;
            cout << actual->dato << "->";
        }
        cout << endl;
    }
};

int main() {
    Lista numeros = Lista();
    numeros.pushBack(2);
    numeros.pushBack(4);
    numeros.pushBack(8);
    numeros.pushBack(16);
    numeros.print();

    while(numeros.removeFront()) {
        numeros.print();
    }

    numeros.pushFront(2);
    numeros.pushFront(4);
    numeros.pushFront(8);
    numeros.pushFront(16);
    numeros.print();

    while(numeros.removeFront());
    numeros.print();

    getch();
}

