#include <iostream>
#include <stack>

using namespace std;
typedef stack<int> Pila;

Pila reemplazar(Pila, int, int);
void imprimir(Pila pila);

int main () {
    Pila pila = Pila();
    pila.push(3);
    pila.push(1);
    pila.push(4);
    pila.push(1);
    pila.push(6);
    
    pila = reemplazar(pila, 1, 9);
    
    imprimir(pila);
}

Pila reemplazar(Pila pila, int viejo, int nuevo) {
    Pila nueva_pila = Pila();
    int top;
    while (!pila.empty()) {
        top = pila.top();
        if (top == viejo) {
            nueva_pila.push(nuevo);
        } else {
            nueva_pila.push(top);
        }
        
        pila.pop();
    }
    
    return nueva_pila;
}

void imprimir(Pila pila) {

    while(!pila.empty()) {
        int numero = pila.top();
        cout << numero;
        pila.pop();
    }
    
    cout << endl;
}

