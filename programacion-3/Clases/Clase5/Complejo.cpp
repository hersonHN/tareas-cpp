#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Complejo {

    public:

    float real;
    float imaginario;
    
    Complejo() {
        real = 0;
        imaginario = 0;
    }
    
    Complejo(float r, float i) {
        real = r;
        imaginario = i;
    }
    
    
    void print() {
        cout << real << " + " << imaginario << "i" << endl;
    }

    string toString() {
        stringstream s;
        s << real << " + " << imaginario << "i" << endl;
        return s.str();
    }
};


