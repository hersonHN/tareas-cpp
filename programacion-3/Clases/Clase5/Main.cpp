#include <iostream>
#include <conio.h>
#include "Complejo.cpp"

using namespace std;

void mostrar(Complejo);
Complejo operator +(Complejo, Complejo);
Complejo operator ++(Complejo &, int); // c++
Complejo operator ++(Complejo &);      // ++c

bool operator ==(Complejo, Complejo);
ostream& operator <<(ostream &, Complejo &a);


int main() {
    Complejo a = Complejo(4, 2);
    Complejo b = Complejo(3, 4);
    
    Complejo c = a + b;
    c.print();

    cout << endl;

    mostrar(++c);
    mostrar(c);
    
    cout << endl;

    mostrar(c++);
    mostrar(c);
    
    cout << endl;
    
    cout << c;
    
    cout << (c == Complejo(1, 2));
    
    
    getch();
}

Complejo operator +(Complejo a, Complejo b) {
     return Complejo(a.real + b.real, a.imaginario + b.imaginario);
}

Complejo operator ++(Complejo &a, int) {
    Complejo c = Complejo(a.real++, a.imaginario);
    return c;
}

Complejo operator ++(Complejo &a) {
    a.real++;
    return a;
}

bool operator ==(Complejo a, Complejo b) {
    return a.real == b.real && a.imaginario == b.imaginario;
}

ostream& operator <<(ostream &stream, Complejo &numero) {
    stream << numero.toString();
    return stream;
}

void mostrar(Complejo x) {
    cout << x;
}
