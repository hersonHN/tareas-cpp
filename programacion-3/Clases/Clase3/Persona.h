#ifndef PERSONA_H
#define PERSONA_H

#include <iostream>
#include <string.h>

using namespace std;

class Persona {

private:
    string nombre;
    string apellido;
    int edad;

public:
   void setNombre(string);
   void setApellido(string);
   void setEdad(int);
   
   string getNombre();
   string getApellido();
   int getEdad();
   
   Persona();
   Persona(string, string, int);
   
   void print();

};

#endif
