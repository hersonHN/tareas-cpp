#include "Persona.h"
#include <iostream>

using namespace std;

using namespace std;

class Dude {

private:
    string nombre;
    string apellido;
    int edad;

public:

    void setNombre(string n) {
        nombre = n;
    }

    string getNombre() {
        return nombre;
    }


    void setApellido(string a) {
        apellido = a;
    }

    string getApellido() {
        return apellido;
    }


    void setEdad(int e) {
        edad = e;
    }

    int getEdad() {
        return edad;
    }

    Dude() {
        nombre = "";
        apellido = "";
        edad = 0;
    }

    Dude(string n, string a, int e) {
        setNombre(n);
        setApellido(a);
        setEdad(e);
    }


    void print() {
        cout << "Nombre: "   << getNombre()   << endl;
        cout << "Apellido: " << getApellido() << endl;
        cout << "Edad: "     << getEdad()     << endl;
    }
};
