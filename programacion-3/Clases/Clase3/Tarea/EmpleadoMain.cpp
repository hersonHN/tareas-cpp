#include <iostream>
#include "Empleado.cpp"
#include "Proletario.cpp"
#include <conio.h>

using namespace std;

int main() {
    Empleado e = Empleado("Michel", -21, -20000);
    e.print();

    Proletario a = Proletario("Herson", 22, 4500);
    a.print();

    getch();
}
