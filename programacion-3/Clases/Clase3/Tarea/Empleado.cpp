#include <iostream>
#include "Empleado.h"


void Empleado::setNombre(string n) {
    nombre = n;
}

string Empleado::getNombre() {
    return nombre;
}

void Empleado::setEdad(int e) {
    if (e < 1 || e > 99) {
        std::cout << "Edad invalida: " << e << std::endl;
    } else {
        edad = e;
    }
}

int Empleado::getEdad() {
     return edad;
}

void Empleado::setSalario(float s) {
    if (s < 0) {
        std::cout << "Salario invalido: " << s << std::endl;
    } else {
        salario = s;
    }
}

float Empleado::getSalario() {
    return salario;
}

Empleado::Empleado() {
    nombre = "";
    edad = 0;
    salario = 0.0;
}

Empleado::Empleado(string n, int e, float s) {
    setNombre(n);
    setEdad(e);
    setSalario(s);
}


void Empleado::print() {
    cout << "Nombre: "   << getNombre()  << endl;
    cout << "Edad: "     << getEdad()    << endl;
    cout << "Salario: "  << getSalario() << endl;
}


