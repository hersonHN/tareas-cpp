#include <iostream>

using namespace std;

class Proletario {

private:
    string nombre;
    int edad;
    float salario;

public:

    void setNombre(string n) {
        nombre = n;
    }

    string getNombre() {
        return nombre;
    }

    void setEdad(int e) {
        edad = e;
    }

    int getEdad() {
        return edad;
    }

    void setSalario(float a) {
        salario = a;
    }

    float getSalario() {
        return salario;
    }

    Proletario() {
        nombre = "";
        edad = 0;
        salario = 0.0;
    }

    Proletario(string n, int e, float s) {
        setNombre(n);
        setEdad(e);
        setSalario(s);
    }


    void print() {
        cout << "Nombre: "   << getNombre()  << endl;
        cout << "Edad: "     << getEdad()    << endl;
        cout << "Salario: "  << getSalario() << endl;
    }
};
