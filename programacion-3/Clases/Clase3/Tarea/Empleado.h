#ifndef EMPLEADO_H
#define EMPLEADO_H

#include <iostream>
#include <string.h>

using namespace std;

class Empleado {

private:
    string nombre;
    int edad;
    float salario;

public:
   void setNombre(string);
   void setEdad(int);
   void setSalario(float);
   
   string getNombre();
   int getEdad();
   float getSalario();
   
   Empleado();
   Empleado(string, int, float);
   
   void print();

};

#endif
