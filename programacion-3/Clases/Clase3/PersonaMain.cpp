#include <iostream>
#include "Persona.cpp"
#include "Dude.cpp"
#include <conio.h>

using namespace std;

int main() {
    Persona d = Persona("Michel", "Mejia", 21);
    d.print();

    Dude p = Dude("Herson", "Salinas", 22);
    p.print();
    
    getch();
}
