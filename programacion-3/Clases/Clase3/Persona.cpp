#include "Persona.h"
#include <iostream>

using namespace std;

void Persona::setNombre(string n) {
    nombre = n;
}

string Persona::getNombre() {
    return nombre;
}


void Persona::setApellido(string a) {
    apellido = a;
}

string Persona::getApellido() {
    return apellido;
}


void Persona::setEdad(int e) {
    edad = e;
}

int Persona::getEdad() {
    return edad;
}

Persona::Persona() {
    nombre = "";
    apellido = "";
    edad = 0;
}

Persona::Persona(string n, string a, int e) {
    setNombre(n);
    setApellido(a);
    setEdad(e);
}


void Persona::print() {
    cout << "Nombre: "   << getNombre()   << endl;
    cout << "Apellido: " << getApellido() << endl;
    cout << "Edad: "     << getEdad()     << endl;
}
