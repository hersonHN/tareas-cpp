#include <iostream>
#include <string.h>

#include "Persona.cpp"

using namespace std;

class Empleado:Persona {

protected:
    float salario;

public:
    Empleado():Persona() {
        salario = 0;
    }

    Empleado(string _nombre, string _apellido, int _edad, float _salario):Persona(_nombre, _apellido, _edad) {
        salario = _salario;
    }
    
    void printEmpleado() {
        print();
        cout << "Salario: " << salario;
    }
    
    void aumentarSalario(float cantidad) {
        salario += cantidad;
    }

};
