#include <iostream>
#include <string.h>
#include <conio.h>

using namespace std;

class Figura {

protected:
    string tipo;
    string nombre;
public:
    Figura(string t) {
        tipo = t;
    }
    
    void print() {
        cout << "Tipo: " << tipo << endl;
        cout << "Nombre: " << nombre << endl;
    }
};

class Figura2D:Figura {
    public:
    using print;
    Figura2D(string n):Figura("Figura 2D") {
        nombre = n;
    }

};


int main() {
    Figura2D c = Figura2D("Cuadrado");
    c.print();
    getch();
}


