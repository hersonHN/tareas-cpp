#include <iostream>
#include <conio.h>

using namespace std;

int main() {
    int dimension, limite;
    
    cout << "Que dimension desea? "; cin >> dimension;
    
    limite = dimension - 1;
    
    for (int x = dimension; x--> 0;) {
        for (int y = dimension; y--> 0;) {
            cout << ((x == 0 || x == limite || y == 0 || y == limite) ? "#" : " ");
        }
        cout << endl;
    }
    
    getch();
}

