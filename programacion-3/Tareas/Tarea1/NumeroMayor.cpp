#include <iostream>
#include <conio.h>


using namespace std;

int mayor(int, int);

int main() {
    int numero1, numero2, numero3;
    
    cout << "Ingrese el primer numero: "; cin >> numero1;
    cout << "Ingrese el segundo numero: "; cin >> numero2;
    cout << "Ingrese el tercer numero: "; cin >> numero3;
    
    cout << "numero mayor: " << mayor(numero1, mayor(numero3, numero2));
    
    getch();
}

int mayor(int n1, int n2){
    return (n1 > n2) ? n1 : n2;
}
