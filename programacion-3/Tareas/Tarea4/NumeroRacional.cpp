#include <iostream>
#include <string.h>
#include <sstream>

using namespace std;

class Fraccion {

public:
    int numerador;
    int denominador;

    Fraccion() {
        numerador = 0;
        denominador = 1;
    }
   
    Fraccion (int n, int d) {
        if (d == 0) {
            cout << "El denominador no puede ser 0" << endl;
            return;
        }
        numerador = n;
        denominador = d;
        simplificar();
    }
    
    int MCD(int n, int d) {
        int menor = (n < d) ? n : d;
        int mayor = (n < d) ? d : n;
        int residuo = menor;
        int resultado = menor;

        while (true){
            resultado = residuo;
            residuo = mayor % menor;
            if (residuo == 0){
                break;
            }
            mayor = menor;
            menor = residuo;
        }
        
        if(resultado > 0){
            return resultado;
        } else {
            return 1;
        }
    }
    
    void simplificar() {
        int mcd = MCD(numerador, denominador);
        numerador = numerador / mcd;
        denominador = denominador / mcd;
    }
    
    string toString() {
        stringstream salida;
        salida << numerador << "/" << denominador;
        return salida.str();
    }
    
    Fraccion sumar(Fraccion uno, Fraccion dos) {
        int deno = uno.denominador * dos.denominador;
        int nume1 = (deno / uno.denominador * uno.numerador);
        int nume2 = (deno / dos.denominador * dos.numerador);
        Fraccion resultado = Fraccion(nume1 + nume2, deno);
        
        return resultado;
    }
    
    Fraccion multiplicar(Fraccion uno, Fraccion dos) {
        Fraccion resultado = Fraccion(uno.numerador   * dos.numerador,
                                      uno.denominador * dos.denominador);
        return resultado;
    }

    double valorDecimal() {
        double dividendo = (double) numerador;
        double divisor   = (double) denominador;
        return dividendo / divisor;
    }
    
    Fraccion operator+(Fraccion dos) {
        return dos.sumar(dos, *this);
    }
    
    Fraccion operator-(Fraccion uno) {
        Fraccion dos = Fraccion(numerador, denominador);
        dos.numerador = dos.numerador * -1;
        return sumar(uno, dos);
    }
    
    Fraccion operator*(Fraccion uno) {
        return multiplicar(uno, *this);
    }

    Fraccion operator/(Fraccion divisor) {
        divisor = Fraccion(divisor.denominador, divisor.numerador);
        return multiplicar(*this, divisor);
    }

    friend ostream &operator<<(ostream &os, Fraccion f) {
        os << f.toString();
        return os;
    }

    friend istream &operator>>(istream &in, Fraccion &f) {
        int numeradorAnterior = f.numerador;
        int denominadorAnterior = f.denominador;
        
        in >> f.numerador;
        in >> f.denominador;

        if (f.denominador == 0) {
            cout << "EL DENOMINADOR NO PUEDE SER CERO!!!" << endl;
            f.numerador   = numeradorAnterior;
            f.denominador = denominadorAnterior;
        }

        return in;
    }

    

    bool operator<(Fraccion f) {
        return (valorDecimal() < f.valorDecimal());
    }

    bool operator>(Fraccion f) {
        return (valorDecimal() > f.valorDecimal());
    }

    bool operator==(Fraccion f) {
        f.simplificar();
        simplificar();

        return (numerador == f.numerador && denominador == f.denominador);
    }
};



