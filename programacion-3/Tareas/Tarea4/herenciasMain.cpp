/*

Herencia: Crear el esquema de herencia que ud crea indicado para representar 

Vehiculos, donde podremos ver vehiculos particulares como:
    Turismo (nombre, marca, modelo, año, Dueño, NumeroPuertas)
    Moto (nombre, marca, modelo, año, Dueño),

y vehiculos publicos como:
    Taxi (nombre, marca, modelo, año, Empresa, NumeroTaxi ) 
    Autobus(nombre, marca, modelo, año, Empresa, NumeroPasajeros)

Realizar una estructura de 3 niveles de Herencia

*/

#include <iostream>
#include <conio.h>
#include "herencias.cpp"

using namespace std;

int main () {
    VehiculoPublico miVehiculoPublico = VehiculoPublico();
    miVehiculoPublico.setNombre("mi vehiculo publico");

    Turismo miTurismo = Turismo();
    miTurismo.setAnio(2013);
    miTurismo.setNombre("mi carro");
    
    cout << miTurismo.getNombre() << " es del anio " << miTurismo.getAnio() << endl;

    
    Moto laMoto = Moto();
    laMoto.setNombre("La moto");
    laMoto.setDuenio("el senior ese");

    cout << laMoto.getNombre() << " es de " << laMoto.getDuenio() << endl;


    Taxi eseTaxi = Taxi();
    eseTaxi.setNombre("Ese taxi");
    eseTaxi.setNumeroTaxi(1337);

    cout << eseTaxi.getNombre() << " tiene por numero " << eseTaxi.getNumeroTaxi() << endl;


    Autobus eseAutobus = Autobus();
    eseAutobus.setNombre("Ese Catul");
    eseAutobus.setNumeroPasajeros(60);

    cout << eseAutobus.getNombre() << " tiene capacidad para "
         << eseAutobus.getNumeroPasajeros() << " pasajeros" << endl;

    getch();
    return 0;
}
