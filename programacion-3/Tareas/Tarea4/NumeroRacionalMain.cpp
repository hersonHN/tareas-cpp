/*
    2. Sobrecarga Operadores:
    Crear una clase llamada NumeroRacional con lo siguiente:

    a. Constructor que simplifique la fraccion y que evite denominador 0
    b. Sobrecargar la suma, resta, multiplicacion y division.
    c. Sobrecargar los operadores relacionales y de igualdad
    d. Sobrecargar los operadores de lectura y escritura de datos
*/

#include <iostream>
#include <string.h>
#include <conio.h>
#include "NumeroRacional.cpp"

using namespace std;

int main() {

    Fraccion unTercio = Fraccion(1, 3);
    Fraccion unCuarto = Fraccion(1, 4);
    
    cout << unCuarto << " + " << unTercio << " = " << (unCuarto + unTercio) << endl;
    cout << unCuarto << " - " << unTercio << " = " << (unCuarto - unTercio) << endl;
    cout << unCuarto << " * " << unTercio << " = " << (unCuarto * unTercio) << endl;
    cout << unCuarto << " / " << unTercio << " = " << (unCuarto / unTercio) << endl;

    cout << "Es " << unCuarto << " mayor que " << unTercio << "?  ";
    cout << ((unCuarto > unTercio) ? "SI" : "NO") << endl;

    cout << "Es " << unCuarto << " menor que " << unTercio << "?  ";
    cout << ((unCuarto < unTercio) ? "SI" : "NO") << endl;

    cout << "Es " << unCuarto << " igual que " << unTercio << "?  ";
    cout << ((unCuarto == unTercio)? "SI" : "NO") << endl;

    cout << "Es " << Fraccion(15,10) << " igual que " << Fraccion(15,10) << "?  ";
    cout << ((Fraccion(15, 10) == Fraccion(15, 10)) ? "SI" : "NO") << endl;

    cout << "Vamos a crear una fraccion! ingrese el numerador, luego " << endl <<
            "presione <enter> y por ultimo ingrese el denominador:   " << endl;

    Fraccion miFraccion = Fraccion();
    cin >> miFraccion;
    cout << "Su fraccion es: " << miFraccion << endl;

    getch();
    return 0;
}


