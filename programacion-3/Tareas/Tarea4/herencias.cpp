#include <iostream>
#include <string.h>

using namespace std;

class Vehiculo {
    private:
    string nombre;
    string marca;
    string modelo;
    string duenio;
    int anio;

    public:
    Vehiculo() {
        nombre = "";
        marca = "";
        modelo = "";
        anio = 0;
    }
    
    // Getters
    string getNombre() { return nombre; }
    string getMarca () { return marca;  }
    string getModelo() { return modelo; }
    string getDuenio() { return duenio; }
    int    getAnio  () { return anio;   }

    // Setters
    void setNombre(string n) { nombre = n; }
    void setMarca (string m) { marca  = m; }
    void setModelo(string m) { modelo = m; }
    void setDuenio(string d) { duenio = d; }
    void setAnio  (int    a) { anio   = a; }

};


class VehiculoParticular: public Vehiculo {
    private:
    string duenio;

    public:
    VehiculoParticular():Vehiculo() {
        duenio = "";
    }

    string getDuenio() { return duenio; }
    void setDuenio (string d) { duenio = d; }
};


class Turismo: public VehiculoParticular {
    private:
    int numeroPuertas;

    public:
    Turismo():VehiculoParticular() {
        numeroPuertas = 0;
    }

    int getNumeroPuertas() { return numeroPuertas; }
    void setNumeroPuertas(int n) { numeroPuertas = n; }
};


class Moto: public VehiculoParticular {
    public:
    Moto():VehiculoParticular() {}
};


class VehiculoPublico: public Vehiculo {
    private:
    string empresa;

    public:
    VehiculoPublico():Vehiculo() {
        empresa = "";
    }

    string getEmpresa() { return empresa; }
    void setEmpresa(string e) { empresa = e; }
};


class Taxi: public VehiculoPublico {
    private:
    int numeroTaxi;

    public:
    Taxi():VehiculoPublico() {
        numeroTaxi = 0;
    }

    int getNumeroTaxi() { return numeroTaxi; }
    void setNumeroTaxi(int n) { numeroTaxi = n; }  
};


class Autobus: public VehiculoPublico {
    private:
    int numeroPasajeros;

    public:
    Autobus():VehiculoPublico() {
        numeroPasajeros = 0;
    }

    int getNumeroPasajeros() { return numeroPasajeros; }
    void setNumeroPasajeros(int n) { numeroPasajeros = n; }  
};


