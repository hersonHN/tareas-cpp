/*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#/
    
a. Sobrecargar el operador de resta para calcular la diferencia en días entre
   una fecha y un numero
b. Sobrecargar el operador -- en sus versiones postfijo y prefijo para restar
   un dia a la fecha
c. Modificar el operador << para escribir fechas en formato largo 5 de agosto
   del 2013, Utilizar un arreglo de constantes para el nombre de los meses

La estructura para las fechas será:

    class fecha {
        int dia;
        int mes;
        int anno;
    };

/#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*/

#include <iostream>
#include "fecha.cpp"

using namespace std;

void diasAnteriores(Fecha f) {
    cout << f << endl << --f << endl;
    cout << f << endl << f-- << endl;
    cout << f << endl        << endl;
}

int main() {
    
    diasAnteriores(Fecha(2000, 8, 8));
    diasAnteriores(Fecha(2000, 5, 1));
    diasAnteriores(Fecha(2000, 1, 1));
    diasAnteriores(Fecha(2000, 3, 1));
    diasAnteriores(Fecha(2001, 3, 1));
    
    
    Fecha inicio = Fecha(2004, 4, 1);
    Fecha final  = Fecha(2004, 4, 10);

    cout << inicio << endl;
    cout << final << endl;
    cout << final - inicio << endl;

    return 0;
}

