#ifndef FECHA_H
#define FECHA_H

#include <iostream>
#include <string.h>
#include <sstream>

using namespace std;

class Fecha {

    private:

    int dia;
    int mes;
    int anio;
    int limiteDias[12];
    string nombreMeses[12];

    void __inicializar();
    string formatear(int);

    public:

    Fecha();
    Fecha(int, int, int);

    // Setters
    bool setDia(int);
    bool setMes(int);
    bool setAnio(int);

    // Getters
    int getDia();
    int getMes();
    int getAnio();

    string printCorto();
    string printLargo();
    
    bool esBisiesto();
    static bool esBisiesto(int);

    // Acciones
    void diaAnterior();
    int diaDelAnio();
    static int diferenciaEnDias(Fecha, Fecha);
    
    // operators

    friend ostream& operator<<(ostream& os, Fecha fecha) {
        os << fecha.printLargo();
        return os;
    }

    Fecha operator--() {
        diaAnterior();
        return *this;
    }

    Fecha operator--(int) {
        diaAnterior();
        return *this;
    }

    int operator-(Fecha uno) {
        return diferenciaEnDias(*this, uno);
    }

};

#endif
