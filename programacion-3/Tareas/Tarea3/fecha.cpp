/*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#/
    
a. Sobrecargar el operador de resta para calcular la diferencia en días entre
   una fecha y un numero
b. Sobrecargar el operador -- en sus versiones postfijo y prefijo para restar
   un dia a la fecha
c. Modificar el operador << para escribir fechas en formato largo 5 de agosto
   del 2013, Utilizar un arreglo de constantes para el nombre de los meses

La estructura para las fechas será:

Class fecha {
    int dia;
    int mes;
    int anno;
};

/#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*/


#include <iostream>
#include <string.h>
#include <sstream>
#include <cmath>
#include "fecha.h"

using namespace std;

Fecha::Fecha() {
    __inicializar();
}

void Fecha::__inicializar() {
    anio = 1900;
    mes = 1;
    dia = 1;

    limiteDias[0]  = 31; /* Enero      */  limiteDias[1]  = 29; /* Febrero   */
    limiteDias[2]  = 31; /* Marzo      */  limiteDias[3]  = 30; /* Abril     */
    limiteDias[4]  = 31; /* Mayo       */  limiteDias[5]  = 30; /* Junio     */
    limiteDias[6]  = 31; /* Julio      */  limiteDias[7]  = 31; /* Agosto    */
    limiteDias[8]  = 30; /* Septiembre */  limiteDias[9]  = 31; /* Octubre   */
    limiteDias[10] = 30; /* Nombiembre */  limiteDias[11] = 31; /* Diciembre */
    
    nombreMeses[0] = "enero";
    nombreMeses[1] = "febrero";
    nombreMeses[2] = "marzo";
    nombreMeses[3] = "abril";
    nombreMeses[4] = "mayo";
    nombreMeses[5] = "junio";
    nombreMeses[6] = "julio";
    nombreMeses[7] = "agosto";
    nombreMeses[8] = "septiembre";
    nombreMeses[9] = "octubre";
    nombreMeses[10] = "noviembre";
    nombreMeses[11] = "diciembre";
}


Fecha::Fecha(int a, int m, int d) {
    __inicializar();

    if (!(setAnio(a) && setMes(m) && setDia(d))) {
        cout << "FECHA INVALIDA (" << a << "-" << m << "-" << d << ")" << endl;
    }
}

// Getters
int Fecha::getAnio() { return anio; }
int Fecha::getMes()  { return mes; }
int Fecha::getDia()  { return dia; }


// Setters
bool Fecha::setAnio(int a) {
    anio = a;
    return true;
}

bool Fecha::setMes(int m) {
    if (m < 0)  return false;
    if (m > 12) return false;

    mes = m;
    return true;
}

bool Fecha::setDia(int d) {
    int indice = mes - 1;

    if (d < 1) return false;

    // validamos el limite de dias
    if (d > limiteDias[indice]) return false;

    // validamos en caso de años bisiestos
    if (mes == 2) {
        bool bisiesto = esBisiesto();
        if ( bisiesto && d > 29) return false;
        if (!bisiesto && d > 28) return false;
    }

    dia = d;

    return true;
}


bool Fecha::esBisiesto() {
    return esBisiesto(anio);
}

bool Fecha::esBisiesto(int a) {
    return ((a % 4 == 0 && a % 100 != 0) || (a % 400 == 0));
}

string Fecha::printCorto() {
    stringstream salida;
    salida << formatear(dia) << "/" << formatear(mes) << "/" << anio;
    return salida.str();
}

string Fecha::printLargo() {
    stringstream salida;
    salida << dia << " de " << nombreMeses[mes - 1] << " de " << anio;
    return salida.str();
}

string Fecha::formatear(int num) {
    stringstream salida;
    salida << (num < 10 ? "0" : "") << num;
    return salida.str();
}

void Fecha::diaAnterior() {
    if (dia > 1) {
        dia  = dia - 1;
        mes  = mes;
        anio = anio;

        return;
    }

    int nuevoDia, nuevoMes, nuevoAnio;

    if (mes == 1) {
        nuevoMes  = 12;
        nuevoAnio = anio - 1;
    } else {
        nuevoMes  = mes - 1;
        nuevoAnio = anio;
    }

    // si se va calcular febrero
    if (nuevoMes == 2) {
        bool bisiesto = esBisiesto(nuevoAnio);
        if (bisiesto == true) {
            nuevoDia = 29;
        } else {
            nuevoDia = 28;
        }
    } else {
        nuevoDia = limiteDias[nuevoMes - 1];
    }

    dia  = nuevoDia;
    mes  = nuevoMes;
    anio = nuevoAnio;
}

int Fecha::diaDelAnio() {
    int dias = 0;
    for (int n = 0; n < mes - 1; n++) {
        if (n == 1) { // Febrero
            dias += (esBisiesto() ? 29 : 28);
        } else {
            dias += limiteDias[mes - 1];
        }
    }
    return dias + dia;
}

int Fecha::diferenciaEnDias(Fecha inicio, Fecha final) {
    int direccion,
        anioInicial = inicio.getAnio(),
        anioFinal   = final.getAnio();

    if (anioInicial == anioFinal) {
        return abs(final.diaDelAnio() - inicio.diaDelAnio());        
    }

    int diasDelAnio = abs(final.diaDelAnio() - inicio.diaDelAnio());

    if (anioInicial > anioFinal) {
        int swap = anioFinal;
        anioFinal = anioInicial;
        anioInicial = swap;
    }

    cout << "Ano inicial: " << anioInicial << endl;
    cout << "Ano final:   " << anioFinal   << endl;

    int dias = 0;
    for (int n = anioInicial; n < anioFinal; n++) {
        dias += esBisiesto(n) ? 366 : 365;
    }


    return dias + diasDelAnio;
}



