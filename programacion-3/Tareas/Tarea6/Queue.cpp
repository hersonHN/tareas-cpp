
#include <iostream>
#include <string.h>

using namespace std;

template <class CLASS>
class Item {

    public:

    CLASS value;
    Item<CLASS> *next;
    Item<CLASS> *prev;

    Item<CLASS>(CLASS v) {
        value = v;
    } 
};



template <class CLASS>
class Queue {

    private:

    int length;
    Item<CLASS> *first;
    Item<CLASS> *last;

    public:

    Queue<CLASS>() {
        length = 0;
        first = 0;
        last = 0;
    }

    void enqueue(const CLASS &value) {

        Item<CLASS> *item = new Item<CLASS>(value);

        if (length == 0) {
            first = item;
            last = item;
        } else {
            item->prev = last;
            last->next = item;
            last = item;
        }

        length++;
    }

    CLASS dequeue() {
        Item<CLASS> *item = first;

        if (item->next != 0) {
            first = item->next;
            first->prev = 0;
        }

        return item->value;
    }

    void print() {
        Item<CLASS> *item;
        item = first;

        while(item != 0) {
            cout << item->value << " ";
            item = item->next;
        }

        cout << endl;
    }

    int size() {
        return length;
    }

};



int main () {

    {
        Queue<string> q = Queue<string>();
        q.enqueue("a"); cout << "Agregado: a" << endl;
        q.enqueue("b"); cout << "Agregado: b" << endl;
        q.enqueue("c"); cout << "Agregado: c" << endl;
        cout << "Contenido: "; q.print();
    }

    cout << endl;

    {
        Queue<int> q = Queue<int>();
        q.enqueue(1); cout << "Agregado: 1" << endl;
        q.enqueue(2); cout << "Agregado: 2" << endl;
        q.enqueue(3); cout << "Agregado: 3" << endl;

        cout << "Removido: " << q.dequeue() << endl;
        cout << "Removido: " << q.dequeue() << endl;
        cout << "Contenido: "; q.print();
    }

}



