
#include <iostream>
#include <string.h>

using namespace std;

template <class CLASS>
class Item {

    public:

    CLASS value;
    Item<CLASS> *next;
    Item<CLASS> *prev;

    Item<CLASS>(CLASS v) {
        value = v;
    }

    void imprimir() {
        cout << value << " ";
    }
};



template <class CLASS>
class Queue {

    private:

    int length;
    Item<CLASS> *first;
    Item<CLASS> *last;

    public:

    Queue<CLASS>() {
        length = 0;
        first = 0;
        last = 0;
    }

    void enqueue(const CLASS &value) {

        Item<CLASS> *item = new Item<CLASS>(value);

        if (length == 0) {
            first = item;
            last = item;
        } else {
            item->prev = last;
            last->next = item;
            last = item;
        }

        length++;
    }

    CLASS dequeue() {

        if (length == 0) return first->value;

        Item<CLASS> *item = first;

        first = item->next;
        first->prev = 0;

        length--;

        return item->value;
    }

    void print() {
        Item<CLASS> *item;
        item = first;

        if (length == 0) return;

        int l = length - 1;
        do {
            item->imprimir();
            item = (item->next);
        } while (l--);

        cout << endl;
    }

    int size() {
        return length;
    }

};



int main () {
    {
        Queue<int> q = Queue<int>();

        q.enqueue(1); cout << "Agregado: 1" << endl;
        q.enqueue(2); cout << "Agregado: 2" << endl;
        q.enqueue(3); cout << "Agregado: 3" << endl;

        cout << "Removido: " << q.dequeue() << endl;
        cout << "Removido: " << q.dequeue() << endl;
        cout << "Contenido: "; q.print();
    }

    cout << endl;

    {
        Queue<string> q = Queue<string>();

        q.enqueue("a"); cout << "Agregado: a" << endl;
        q.enqueue("b"); cout << "Agregado: b" << endl;
        q.enqueue("c"); cout << "Agregado: c" << endl;

        cout << "Contenido: "; q.print();
    }
}



