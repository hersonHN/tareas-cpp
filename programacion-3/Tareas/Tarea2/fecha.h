#ifndef FECHA_H
#define FECHA_H

#include <iostream>
#include <string.h>

using namespace std;

class Fecha {

    private:

    int dia;
    int mes;
    int anio;
    int limiteDias[12];
    string nombreMeses[12];

    void __inicializar();
    string formatear(int);

    public:

    Fecha();
    Fecha(int, int, int);

    // Setters
    bool setDia(int);
    bool setMes(int);
    bool setAnio(int);

    // Getters
    int getDia();
    int getMes();
    int getAnio();

    string printCorto();
    string printLarga();
    
    bool esBisiesto();

};

#endif
