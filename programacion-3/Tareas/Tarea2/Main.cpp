#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <algorithm>
#include "Empleado.cpp"


using namespace std;

Empleado nuevoEmpleado();

bool comparar(const Empleado& a, const Empleado& b) {
    return a.salario > b.salario;
}


int main() {

    int limite = 5;
    Empleado empleados[5];
    std::vector<Empleado> vec;

    for (int x = 0; x < limite; x++) {
        vec.push_back( nuevoEmpleado() );
    }
    
    sort(vec.begin(), vec.end(), comparar);
    
    cout << "Empleados Ordenados: " << endl;
    for (int n = 0; n < limite; n++) {
        vec[n].printEmpleado();

        cout << endl;
    }

    getch();
    return 0;
}

Empleado nuevoEmpleado() {
    Empleado empleado;
    
    string nombre;
    string apellido;
    int edad;
    float salario;

    Fecha fecha;

    int dia;
    int mes;
    int anio;

    cout << "Ingrese nombre del empleado: "; cin >> nombre;
    cout << "Ingrese apellido del empleado: "; cin >> apellido;
    cout << "Ingrese edad del empleado: "; cin >> edad;
    cout << "Ingrese salario del empleado: "; cin >> salario;

    cout << "Fecha de nacimiento:" << endl;
    cout << "Ingrese el anio (en numeros): "; cin >> anio;
    cout << "Ingrese el mes (1 a 12): "; cin >> mes;
    cout << "Ingrese el dia (en numeros): "; cin >> dia;

    fecha = Fecha(anio, mes, dia);

    empleado = Empleado(nombre, apellido, edad, salario);
    empleado.setFechaNacimiento(fecha);

    cout << endl;

    return empleado;
}



