#include <iostream>
#include <string.h>
#include <sstream>

#include "fecha.h"

using namespace std;

Fecha::Fecha() {
    __inicializar();
}

void Fecha::__inicializar() {
    anio = 1900;
    mes = 1;
    dia = 1;

    limiteDias[0]  = 31; /* Enero      */   limiteDias[1]  = 29; /* Febrero    */
    limiteDias[2]  = 31; /* Marzo      */   limiteDias[3]  = 30; /* Abril      */
    limiteDias[4]  = 31; /* Mayo       */   limiteDias[5]  = 30; /* Junio      */
    limiteDias[6]  = 31; /* Julio      */   limiteDias[7]  = 31; /* Agosto     */
    limiteDias[8]  = 30; /* Septiembre */   limiteDias[9]  = 31; /* Octubre    */
    limiteDias[10] = 30; /* Nombiembre */   limiteDias[11] = 31; /* Diciembre  */
    
    nombreMeses[0] = "enero";
    nombreMeses[1] = "febrero";
    nombreMeses[2] = "marzo";
    nombreMeses[3] = "abril";
    nombreMeses[4] = "mayo";
    nombreMeses[5] = "junio";
    nombreMeses[6] = "julio";
    nombreMeses[7] = "agosto";
    nombreMeses[8] = "septiembre";
    nombreMeses[9] = "octubre";
    nombreMeses[10] = "noviembre";
    nombreMeses[11] = "diciembre";

}


Fecha::Fecha(int a, int m, int d) {
    __inicializar();

    if (!(setAnio(a) && setMes(m) && setDia(d))) {
        cout << "FECHA INVALIDA" << endl;
    }
}

// Setters
bool Fecha::setAnio(int a) {
    anio = a;
    return true;
}

bool Fecha::setMes(int m) {
    if (m < 0)  return false;
    if (m > 12) return false;

    mes = m;
    return true;
}

bool Fecha::setDia(int d) {
    int indice = mes - 1;

    if (d < 1) return false;

    // validamos el limite de dias
    if (d > limiteDias[indice]) return false;

    // validamos en caso de años bisiestos
    if (mes == 2) {
        bool bisiesto = esBisiesto();
        if ( bisiesto && d > 29) return false;
        if (!bisiesto && d > 28) return false;
    }

    dia = d;

    return true;
}


bool Fecha::esBisiesto() {
    return ((anio % 4 == 0) && (anio % 1000 != 0));
}

string Fecha::printCorto() {
    stringstream salida;
    salida << formatear(dia) << "/" << formatear(mes) << "/" << anio;
    return salida.str();
}

string Fecha::printLarga() {
    stringstream salida;
    salida << dia << " de " << nombreMeses[mes - 1] << " de " << anio;
    return salida.str();
}

string Fecha::formatear(int num) {
    stringstream salida;
    salida << (num < 10 ? "0" : "") << num;
    return salida.str();
}




