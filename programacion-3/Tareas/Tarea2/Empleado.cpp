#include <iostream>
#include <string.h>

#include "Persona.cpp"
#include "Fecha.cpp"

using namespace std;

class Empleado:Persona {

protected:
    Fecha fechaNacimiento;

public:
    float salario;
    
    Empleado():Persona() {
        salario = 0;
    }

    Empleado(string _nombre, string _apellido, int _edad, float _salario):Persona(_nombre, _apellido, _edad) {
        salario = _salario;
    }

    void setSalario(float s) {
        salario = s;
    }

    float getSalario() {
        return salario;
    }

    void setFechaNacimiento(Fecha f) {
        fechaNacimiento = f;
    }

    Fecha getFechaNacimiento() {
        return fechaNacimiento;
    }
    
    void printEmpleado() {
        print();
        cout << "Salario: " << salario << endl;
        cout << "Fecha de Nacimiento: " << fechaNacimiento.printLarga() << endl;
    }
    
    void aumentarSalario(float cantidad) {
         salario += cantidad;
    }


};
