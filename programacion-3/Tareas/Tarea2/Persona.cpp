#include <iostream>

using namespace std;

class Persona {

private:
    string nombre;
    string apellido;
    int edad;

public:

    Persona() {
        nombre = "";
        apellido = "";
        edad = 0;
    }

    Persona(string n, string a, int e) {
        setNombre(n);
        setApellido(a);
        setEdad(e);
    }


    void setNombre(string n) {
         nombre = n;
    }

    string getNombre() {
         return nombre;
    }

    void setApellido(string a) {
         apellido = a;
    }

    string getApellido() {
         return apellido;
    }


    void setEdad(int e) {
         edad = e;
    }

    int getEdad() {
         return edad;
    }

    void print() {
        cout << "Nombre: "   << getNombre()   << endl;
        cout << "Apellido: " << getApellido() << endl;
        cout << "Edad: "     << getEdad()     << endl;
    }
};
