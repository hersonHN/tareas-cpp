#include <iostream>
#include <conio.h>
#include "Hora.cpp"

using namespace std;

int main () {
    {
        Hora h = Hora(12, 59, 59);

        cout << h << endl;
        h += 9;
        cout << h << endl;
    }
    cout << endl;


    {
        cout << "Preincremento: " << endl;
        Hora h2 = Hora(12, 49, 50);
        cout << h2 << endl;
        cout << ++h2 << endl;
        cout << h2 << endl;
    }
    cout << endl;

    {
        cout << "Postincremento: " << endl;
        Hora h1 = Hora(12, 49, 50);
        cout << h1 << endl;
        cout << h1++ << endl;
        cout << h1 << endl;
    }
    cout << endl;

    {
        cout << (Hora(20, 20, 20) > Hora(13, 20, 10)) << endl;
        cout << (Hora(20, 20, 20) > Hora(21, 20, 20)) << endl;
    }

    getch();
}
