#include <iostream>
using namespace std;

class NumeroTelefonico {
    private:

    int area, codCiudad, linea;

    public:

    NumeroTelefonico() {
        area = 0;
        codCiudad = 0;
        linea = 0;
    }
    NumeroTelefonico(int a, int c, int l) {
        area = a;
        codCiudad = c;
        linea = l;
    }

    friend ostream &operator<<( ostream &strm, NumeroTelefonico &x ) {
        strm << "(" << x.area << ")"
             << "/" << x.codCiudad
             << "-" << x.linea;
        return strm;
    }

    friend istream &operator>>( istream &is, NumeroTelefonico &x ) {
        is >> x.area >> x.codCiudad >> x.linea;
        return is;
    }


};

