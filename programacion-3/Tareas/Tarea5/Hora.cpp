#include <iostream>
#include <string.h>
#include <sstream>

using namespace std;

class Hora {
    private:
    double horas, minutos, segundos;

    public:
    Hora(double h, double m, double s) {
        horas = h;
        minutos = m;
        segundos = s;
    }

    void incrementar() {
        segundos = segundos + 1;

        if (segundos == 60) {
            segundos = 0;
            minutos++;
        }

        if (minutos == 60) {
            minutos = 0;
            horas++;
        }

        if (horas == 24) {
            horas = 0;
        }
    }

    Hora clone () {
        return Hora(horas, minutos, segundos);
    }

    string toString() {
        stringstream salida;
        salida << horas << ":"
               << minutos << ":"
               << segundos;

        return salida.str();
    }

    friend ostream &operator<<( ostream &os, Hora &h) {
        os << h.toString();
        return os;
    }

    friend istream &operator>>( istream &is, Hora &h ) {
        cout << "Ingrese horas, minutos y segundos: " << endl;
        is >> h.horas >> h.minutos >> h.segundos;
        return is;
    }

    Hora &operator++() {
        incrementar();
        return *this;
    }

    Hora &operator++(int) {
        Hora hora = clone();
        Hora *h = &hora;
        incrementar();
        return *h;
    }

    bool operator>(Hora that) {
        return ((horas * 100 * 100) + (minutos * 100) + segundos) >
               ((that.horas * 100 * 100) + (that.minutos * 100) + that.segundos);
    }

    Hora operator+=(int cantidad) {
        while(cantidad-- > 0)
            incrementar();
        return *this;
    }
};


