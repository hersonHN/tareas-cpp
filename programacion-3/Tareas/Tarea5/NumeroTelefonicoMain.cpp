/*
Crear una clase NumeroTelefonico
- Atributos: Area, CodCiudad, Linea
- Sobrecargar Los siguientes Operadores:
    - << para imprimir el numero: (Area)CodCiudad-Linea
    - >> para leer cada Atributo
*/

#include <iostream>
#include <conio.h>
#include "NumeroTelefonico.cpp"

using namespace std;

int main () {
    NumeroTelefonico n = NumeroTelefonico();
    cout << "Ingrese el codigo de area, "
            "luego el codigo de ciudad "
            "y por ultimo el # de linea " << endl;
    cin >> n;
    cout << n << endl;

    getch();

    return 0;
}

