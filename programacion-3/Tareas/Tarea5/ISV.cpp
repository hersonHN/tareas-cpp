/*
    Crear una aplicación que solicite el nombre y subtotal,
    debe guardar el porcentaje de ISV en una constante
    al final mostrar el nombre, Subtotal, ISV y Total.
*/

#include <iostream>
#include <conio.h>
#include <string.h>

using namespace std;

const double ISV = 0.12;

int main () {

    string nombre;
    double subtotal, impuesto, total;

    cout << "Ingrese nombre del producto: "; cin >> nombre;
    cout << "Ingrese precio del producto: "; cin >> subtotal;

    impuesto = subtotal * ISV;
    total = subtotal + impuesto;

    cout << "Producto: " << nombre   << endl;
    cout << "Subtotal: " << subtotal << endl;
    cout << "ISV:      " << impuesto << endl;
    cout << "Total:    " << total    << endl;

    getch();
    return 0;
}
