
#include <iostream>

using namespace std;

template <class CLASS>
class Arreglo {
    public:
    CLASS *arreglo;

    Arreglo(int tamanio) {
        CLASS arr[tamanio];
        arreglo = arr;
    }

    CLASS &operator[] (int numero) {
        return arreglo[numero];
    }
};


int main () {
    Arreglo<int> a = Arreglo<int>(10);
    a[1] =   5;
    a[2] =  25;
    a[3] = 125;

    cout << a[2] << endl;

    return 0;
}

