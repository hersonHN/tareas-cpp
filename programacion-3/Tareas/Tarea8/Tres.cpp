
#include <iostream>
#include <string.h>

using namespace std;

template <class CLASS>
class Nodo {

    public:

    CLASS valor;
    Nodo<CLASS> *siguiente;
    Nodo<CLASS> *anterior;

    Nodo<CLASS>(CLASS v) {
        valor = v;
    }

    void imprimir() {
        cout << valor << " ";
    }
};



template <class CLASS>
class Lista {

    private:

    int length;
    Nodo<CLASS> *primero;
    Nodo<CLASS> *ultimo;

    public:

    Lista<CLASS>() {
        length = 0;
        primero = 0;
        ultimo = 0;
    }

    void enQueue(const CLASS &valor) {

        Nodo<CLASS> *nodo = new Nodo<CLASS>(valor);

        if (length == 0) {
            primero = nodo;
            ultimo = nodo;
        } else {
            nodo->anterior = ultimo;
            ultimo->siguiente = nodo;
            ultimo = nodo;
        }

        length++;
    }

    CLASS deQueue() {

        if (length == 0) return primero->valor;

        Nodo<CLASS> *nodo = primero;

        primero = nodo->siguiente;
        primero->anterior = 0;

        length--;

        return nodo->valor;
    }

    void printInverso() {
        Nodo<CLASS> *nodo;
        nodo = ultimo;
        if (length == 0) return;

        int l = length - 1;
        do {
            nodo->imprimir();
            nodo = (nodo->anterior);
        } while (l--);

        cout << endl;
    }


    void print() {
        Nodo<CLASS> *nodo;
        nodo = primero;

        if (length == 0) return;

        int l = length - 1;
        do {
            nodo->imprimir();
            nodo = (nodo->siguiente);
        } while (l--);

        cout << endl;
    }

    int size() {
        return length;
    }

    CLASS get(int index) {
        Nodo<CLASS> *nodo;
        nodo = primero;
 
        for (int i = 0; i < index; i++) {
            nodo = (nodo->siguiente);
        }

        return nodo->valor;
    }
    
    void set(int index, CLASS val) {
        Nodo<CLASS> *nodo;
        nodo = primero;
        for (int i = 0; i < index; i++) {
            nodo = (nodo->siguiente);
        }

        nodo->valor = val;
    }

};



int main () {
    {
        Lista<int> c = Lista<int>();

        c.enQueue(1); cout << "Agregado: 1" << endl;
        c.enQueue(2); cout << "Agregado: 2" << endl;
        c.enQueue(3); cout << "Agregado: 3" << endl;
        
        c.printInverso();
    }

}



