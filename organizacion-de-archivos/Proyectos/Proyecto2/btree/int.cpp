
#ifndef __INT_CPP__
#define __INT_CPP__

#include <tgmath.h>
using namespace std;

unsigned char* encode_int(int entrada) {
    int limit = sizeof(entrada);
    unsigned char* salida = new unsigned char[limit];

    for (int x = 0; x < limit; x++) {
        salida[x] = (entrada % 256); 
        entrada = entrada / 256;
    }

    return salida;
}


int decode_int(unsigned char entrada[]) {
    int salida = 0;
    int valor = 0;
    int limit = sizeof(salida);

    for (int x = 0; x < limit; x++) {
        valor = (int) entrada[x];
        salida += valor * pow(256, x);
    }
    return salida;
}

#endif

