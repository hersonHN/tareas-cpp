
#ifndef __BTREE_CPP__
#define __BTREE_CPP__

using namespace std;

#include <deque>
#include "io.cpp"

NodosProcesados procesar_nodos(BTreeNode *, BTreeNode *, int, int);
void procesar_punteros(BTreeNode *, BTreeNode *, int);

BTree::BTree(int tam, string arch) {
    this->tamanio_nodos = tam;
    this->archivo = arch;
    this->vacio = 1;

    this->cargar_header();
}

void BTree::cargar_header() {
    Header header = {1, this->tamanio_nodos, 1};
    this->padding = sizeof(header); // Numero de bytes que se "saltaran" del header del file

    leer_del_archivo<Header *>(this->archivo, &header, sizeof(header), 0);

    this->tamanio_nodos = header.tamanio_nodos;
    this->raiz = new BTreeNode(this);

    this->raiz->cargar(header.indice_raiz);
    this->raiz->indice = header.indice_raiz;
    this->raiz->guardar();
    this->cantidad_de_nodos = header.cantidad_de_nodos;

    this->raiz->posicion_en_la_fila = 0;
}

void BTree::guardar_header() {
    Header header = {0, 0, 0};
    header.indice_raiz = this->raiz->indice;
    header.tamanio_nodos = this->tamanio_nodos;
    header.cantidad_de_nodos = this->cantidad_de_nodos;
    
    escribir_al_archivo<Header *>(this->archivo, &header, sizeof(header), 0);
}

int BTree::buscar(int valor) {
    return this->buscar(valor, this->raiz);
}

int BTree::buscar(int valor, BTreeNode* nodo) {
    // cargando el nodo en memoria
    nodo->cargar();
    
    // Buscando entre los valores
    for (int x = 0; x < nodo->tamanio; x++) {
        if (nodo->valores[x] == valor) {
            return nodo->direcciones[x];
        }
    }

    if (nodo->es_hoja()) {
        return -1;
    }

    // eligiendo un puntero para seguir buscando ahi
    int idx     = nodo->indice_del_valor(valor);
    int puntero = nodo->punteros[idx];
    if (puntero == -1) {
        return -1;
    }
    BTreeNode * nuevoNodo = new BTreeNode(this);
    nuevoNodo->cargar(puntero);
    return this->buscar(valor, nuevoNodo);
}


int BTree::agregar(int valor, int direccion) {
    deque<BTreeNode *> nodos = deque<BTreeNode *>();
    BTreeNode * nodo = raiz;
    BTreeNode * padre;
    nodos.push_back(raiz);

    int salida_de_emergencias = 10;

    //cout << "probando si el nodo es hoja " << endl;
    
    // poniendo el nodo hoja como ultimo en la lista
    while(!nodo->es_hoja()) {
        nodo = nodos[nodos.size() - 1];
        //cout << "valor? " << valor << endl;
        //cout << "El nodo no es hoja " << endl;
        int idx = nodo->indice_del_valor(valor);
        //cout << "Este es el indice: " << idx << endl;
        //cout << "esta aqui: " << endl;
        //nodo->dump();
        int index = nodo->punteros[idx];
        nodo = new BTreeNode(this);
        nodo->cargar(index);
        
        nodos.push_back(nodo);
    }
    nodo = nodos[nodos.size() - 1];

    if (nodo->tiene_espacio()) {
        nodo->agregar(valor, direccion);
        nodo->guardar();
        return 0;
    }
    if (nodo == NULL) {
        return 0;
    }

    NodosProcesados nodos_procesados = this->dividir_nodo(nodo, valor, direccion);

    BTreeNode * nodoPadre;
    this->crearNuevaRaiz(nodo, nodos, nodos_procesados);

    valor = nodos_procesados.valor_a_promover;
    direccion = nodos_procesados.direccion_a_promover;

    nodos_procesados.izquierdo->guardar();
    nodos_procesados.derecho->guardar();
    

    do {
        nodo = nodos[nodos.size() - 1];
        padre = nodos[nodos.size() - 2];
        
        if (nodo->tiene_espacio()) {
            this->promover(nodos_procesados, padre, valor, direccion);

            nodos_procesados.derecho->dump();
            //nodos_procesados = this->dividir_nodo(nodo, valor, direccion);
            //this->crearNuevaRaiz(padre, nodos, nodos_procesados);
        }
           
        if (nodos.size() > 0) {
            nodos.pop_back();
        }
    } while (!nodo->tiene_espacio());


    return 0;
}

void BTree::promover(NodosProcesados nodos_procesados, BTreeNode* nodoPadre, int valor, int direccion) {
    nodoPadre->agregar(valor, direccion);
    int idx = nodoPadre->indice_del_valor(valor);
    this->procesar_punteros(nodos_procesados.izquierdo, nodos_procesados.derecho, nodos_procesados.mitad);

    int i = this->siguiente_vacio(nodoPadre);
    nodoPadre->punteros[i] = nodos_procesados.derecho->indice;
    nodoPadre->guardar();
}

void BTree::crearNuevaRaiz(BTreeNode* nodo, deque<BTreeNode *> &nodos, NodosProcesados nodos_procesados) {
    if (!nodo->es_raiz()) return;

    BTreeNode* r = this->nuevoNodo();
    this->raiz = r;
    this->guardar_header();

    int ii = this->siguiente_vacio(r);
    r->punteros[ii] = nodo->indice;
    nodos.push_front(r);
}

NodosProcesados BTree::dividir_nodo(BTreeNode * nodo, int valor, int direccion) {
    BTreeNode * nodoOrigen = nodo;
    BTreeNode * nodoDestino = this->nuevoNodo();
    
    NodosProcesados nodos_procesados = {};
    nodos_procesados.error = false;

    int tamanio = nodoOrigen->tamanio + 1;
    int * valores     = new int[tamanio];
    int * direcciones = new int[tamanio];

    int contador = 0;
    bool enviado = false;
    for (int x = 0; x < nodoOrigen->tamanio; x++) {
        valores[contador] = nodoOrigen->valores[x];
        direcciones[contador] = nodoOrigen->direcciones[x];

        if (valor < valores[x] && !enviado) {
            valores[contador] = valor;
            direcciones[contador] = direccion;

            valores[contador + 1] = nodoOrigen->valores[x];
            direcciones[contador + 1] = nodoOrigen->direcciones[x];

            contador++;
            enviado = true;
        }

        contador++;
    }

    if (!enviado) {
        valores[contador] = valor;
        direcciones[contador] = direccion;
    }

    int mitad_indice = ((nodoOrigen->tamanio - 1) / 2) + 1;
    nodos_procesados.mitad = mitad_indice;
    nodos_procesados.izquierdo = nodoOrigen;
    nodos_procesados.derecho = nodoDestino;
    nodos_procesados.valor_a_promover = valores[mitad_indice];
    nodos_procesados.direccion_a_promover = direcciones[mitad_indice];

    for (int x = 0; x < nodoOrigen->tamanio; x++) {
        if (x < mitad_indice) {
            nodoOrigen->valores[x] = valores[x];
            nodoOrigen->direcciones[x] = direcciones[x];
        } else {
            nodoOrigen->valores[x] = 0;
            nodoOrigen->direcciones[x] = 0;
        }
    }

    for (int x = 0; x < nodoDestino->tamanio; x++) {
        if (x < mitad_indice) {
            nodoDestino->direcciones[x] = direcciones[mitad_indice + x + 1];
            nodoDestino->valores[x]     = valores[mitad_indice + x + 1];
        } else {
            nodoDestino->direcciones[x] = 0;
            nodoDestino->valores[x] = 0;
        }
    }

    return nodos_procesados;
}


void BTree::procesar_punteros(BTreeNode * nodoA, BTreeNode * nodoB, int indice_fila) {
    int cantidad_punteros = nodoA->tamanio + 1;
    indice_fila = indice_fila; // posicion en la fila del nodo nuevo

    int contador = 0;
    for (int x = indice_fila; x < cantidad_punteros; x++) {
        nodoB->punteros[contador] = nodoA->punteros[x];
        contador++;
    }

    for (int x = indice_fila; x < cantidad_punteros; x++) {
        nodoA->punteros[x] = 0;
    }
}

int BTree::siguiente_vacio(BTreeNode* nodo) {
    for (int x = 0; x < nodo->tamanio + 1; x++) {
        if (nodo->punteros[x] == 0) {
            return x;
        }
    }
    return -1;
}

BTreeNode * BTree::nuevoNodo() {
    this->cantidad_de_nodos++;
    BTreeNode * nodo = new BTreeNode(this);
    nodo->indice = this->cantidad_de_nodos;

    return nodo;
}

void BTree::promover(NodosProcesados nodos_procesados, int indice, int vuelta) {
    BTreeNode * nodo;
    if (nodos_procesados.padre != NULL) {
        nodo = nodos_procesados.padre;
    } else {
        nodo = this->nuevoNodo();
    }
    nodo->punteros[indice] = nodos_procesados.izquierdo->indice;
    nodo->punteros[indice + 1] = nodos_procesados.derecho->indice;

    nodo->agregar(
        nodos_procesados.valor_a_promover,
        nodos_procesados.direccion_a_promover
    );

    nodo->guardar();

    if (vuelta == 1) { // Si se esta promoviendo hacia la raiz
        this->raiz = nodo;
        this->guardar_header();
    }

    nodos_procesados.izquierdo->guardar();
    nodos_procesados.derecho->guardar();
}


#endif

