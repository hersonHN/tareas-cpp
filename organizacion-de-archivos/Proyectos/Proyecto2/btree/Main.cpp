
#include <iostream>

#include "tipos.cpp"
#include "btree-node.cpp"
#include "btree.cpp"


using namespace std;

void probar(BTree*, int, int);

int main() {
    string archivo = "ke-ase.data";

    crear_archivo(archivo);
    BTree* arbol = new BTree(4, archivo);

    int maximo = 17;
    for (int x = 1; x <= maximo; x++) {
        arbol->agregar(x, x * 100);
        cout << "Agregando: " << x << endl;
        arbol->raiz->r_dump();
        cout << endl;
    }
    for (int x = 1; x <= maximo; x++) {
        probar(arbol, x, x * 100);
    }

    return 0;
}

void probar(BTree* arbol, int key, int value) {
    int respuesta = arbol->buscar(key);
    if (respuesta == value) {
        cout << "Valor " << key << " encontrado" << endl;
    } else {
        cout << "VALOR " << key << " NO ENCONTRADO, SE ENCONTRO " << respuesta << endl;
    }
}

