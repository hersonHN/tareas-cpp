
// Abstrae las funciones de Lectura/Escritura del archivo de base de datos, de modo que
// no se haga referencia a el fuera de estas funciones.

#ifndef __IO_CPP__
#define __IO_CPP__

#include <iostream>
#include <fstream>

using namespace std;

// Cuerpo de Funciones

template <typename TIPO>
void escribir_al_archivo(const string& archivo, TIPO data, int tamanio, int posicion) {
    // si el archivo no existe se crea
    ofstream out(archivo.c_str(), ios::in | ios::out | ios::binary | ios::ate);
    out.seekp(posicion);
    
    // escribir el struct en formato de un arreglo de caracteres
    //for (int x = 0; x < tamanio; x++) {
    out.write(reinterpret_cast<char *>(data), tamanio);
    //}
    out.close();
}

template <typename TIPO>
void leer_del_archivo(const string& archivo, TIPO data, int tamanio, int posicion) {
    // intenta leer el archivo especificado
    ifstream in(archivo.c_str());
    in.seekg(posicion);

    // leer el struct en formato de un arreglo de caracteres
    // si el archivo no existe el struct no se sobreescribe
    in.read(reinterpret_cast<char *>(data), tamanio);
    in.close();
}


#ifndef fn_crear_archivo__
#define fn_crear_archivo__
void crear_archivo(const string& archivo) {
    ofstream out(archivo.c_str());
}
#endif

#endif


