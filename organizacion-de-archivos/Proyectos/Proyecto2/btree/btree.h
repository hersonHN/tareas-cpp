
#ifndef __BTREE_H__
#define __BTREE_H__

#include <deque>

using namespace std;


class BTree {
    private:

    int tamanio_nodos;
    int vacio;
    string archivo;
    int padding;

    public:
    int cantidad_de_nodos;
    BTreeNode* raiz;

    BTree(int, string);

    int getTamanio() {
        return tamanio_nodos;
    }

    string getArchivo() {
        return archivo;
    }

    int getPadding() {
        return padding;
    }

    void cargar_header();
    void guardar_header();

    void promover(NodosProcesados, int, int);


    int buscar(int); // devuelve la direccion segun el valor
    int buscar(int, BTreeNode*);
    int agregar(int, int); // devuelve un codigo de estado
    Respuesta agregar(int, int, BTreeNode*, int);

    int indice_del_valor(BTreeNode*, int);
    int siguiente_vacio(BTreeNode*);
    BTreeNode* nuevoNodo();

    NodosProcesados dividir_nodo(BTreeNode*, int, int);
    void procesar_punteros(BTreeNode*, BTreeNode*, int);
    void promover(NodosProcesados, BTreeNode*, int, int);
    void crearNuevaRaiz(BTreeNode*, deque<BTreeNode *> &, NodosProcesados);

};

#endif

