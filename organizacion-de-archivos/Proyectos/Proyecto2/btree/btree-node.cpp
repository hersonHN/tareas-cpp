
#ifndef __BTREE_NODE_CPP__
#define __BTREE_NODE_CPP__

#include <iostream>
#include <vector>
#include "btree.h"
#include "int.cpp"
#include "varios.cpp"
#include "io.cpp"

using namespace std;

int tamanio = 4;

class BTreeNode {
    private:
    BTree* padre;

    public:
    
    int tamanio;
    int indice; // indice del nodo actual
    int* valores; // indices de los registros
    int* direcciones; // Punteros a los regisros
    int* punteros; // Punteros a otros nodos
    
    int posicion_en_la_fila;

    BTreeNode(BTree* p) {
        this->padre = p;
        int tam = p->getTamanio();
        
        this->tamanio     = tam;
        this->valores     = new int[tam];
        this->direcciones = new int[tam];
        this->punteros    = new int[tam + 1];

        int x;
        for (x = tamanio;     x--> 0;) { this->valores[x]     = 0; }
        for (x = tamanio;     x--> 0;) { this->direcciones[x] = 0; }
        for (x = tamanio + 1; x--> 0;) { this->punteros[x]    = 0; }
    }
    
    int totalSize() {
        int _indice = 1;
        int _valores = this->tamanio;
        int _direcciones = this->tamanio;
        int _punteros = this->tamanio + 1;
       
        return _indice + _valores + _direcciones + _punteros;
    }

    void dump(string tab) {
        int x;
        cout << tab << "Indice: " << indice << endl;
        
        // valores
        cout << tab << "Valores: [ ";
        for (x = 0; x < this->tamanio; x++) {
            cout << valores[x] << " ";
        }
        cout << "]" << endl;

        // direcciones
        cout << tab << "Direcciones: [ ";
        for (x = 0; x < this->tamanio; x++) {
            cout << direcciones[x] << " ";
        }
        cout << "]" << endl;

        // punteros
        cout << tab << "Punteros: [ ";
        for (x = 0; x < this->tamanio + 1; x++) {
            cout << punteros[x] << " ";
        }
        cout << "]" << endl;
    }

    void dump() {
        this->dump(string(""));
    }

    void r_dump(string tab) {
        this->dump(tab);
        string anidado = tab + string("  ");
        int tam = this->tamanio + 1;
        BTreeNode* hijo;
        for (int x = 0; x < tam; x++) {
            int puntero = this->punteros[x];
            if (puntero > 0) {
                hijo = new BTreeNode(this->padre);
                hijo->cargar(puntero);
                hijo->r_dump(anidado);
            }
        }
    }

    void r_dump() {
        this->r_dump(string(""));
    }

    int totalSizeSerialized() {
        return totalSize() * 4;
    }

    int* toArray() {
        int tam = totalSize();
        int* resultado;
        int pos_actual = 0;

        resultado = new int[tam];

        resultado[0] = this->indice;

        pos_actual = 1;
        for (int x = 0; x < this->tamanio; x++) {
            resultado[x + pos_actual] = valores[x];
        }

        pos_actual += this->tamanio;
        for (int x = 0; x < this->tamanio; x++) {
            resultado[x + pos_actual] = direcciones[x];
        }

        pos_actual += this->tamanio;
        for (int x = 0; x < (this->tamanio + 1); x++) {
            resultado[x + pos_actual] = punteros[x];
        }

        return resultado;
    }

    vector<unsigned char> serialize() {
        int numero;
        int* resultado;
        int tam = this->totalSize();
        resultado = new int[tam];
        resultado = this->toArray();

        vector<unsigned char> serie = vector<unsigned char>();
        unsigned char* chunk = new unsigned char[4];
    
        for (int y = 0; y < tam; y++) {
            numero = resultado[y];
            chunk = encode_int(numero);
            for (int x = 0; x < 4; x++) {
                serie.push_back(chunk[x]);
            }
        }

        return serie;
    }

    void deserialize(unsigned char * datos_a_leer) {
        int posicion = 0;
        int limite;
        int x, y;
        unsigned char* chunk = new unsigned char[4];
        int decoded;

        // Los primeros 4 caracteres son el indice (este no se lee)
        limite = 4;
        chunk[0] = datos_a_leer[0];
        chunk[1] = datos_a_leer[1];
        chunk[2] = datos_a_leer[2];
        chunk[3] = datos_a_leer[3];

        decoded = decode_int(chunk);
        this->indice = decoded;

        // Los proximos N caracteres son los valores
        // { N = this->tamanio }
        y = 0;
        posicion = limite;
        limite += this->tamanio * 4;
        for (x = posicion; x < limite; x += 4) {
            chunk[0] = datos_a_leer[x + 0];
            chunk[1] = datos_a_leer[x + 1];
            chunk[2] = datos_a_leer[x + 2];
            chunk[3] = datos_a_leer[x + 3];

            decoded = decode_int(chunk);
            this->valores[y] = decoded;
            y++;
        }

        // Los proximos N caracteres son las direcciones de los registros
        // { N = this->tamanio }
        y = 0;
        posicion = limite;
        limite += this->tamanio * 4;
        for (x = posicion; x < limite; x += 4) {
            chunk[0] = datos_a_leer[x + 0];
            chunk[1] = datos_a_leer[x + 1];
            chunk[2] = datos_a_leer[x + 2];
            chunk[3] = datos_a_leer[x + 3];

            decoded = decode_int(chunk);
            this->direcciones[y] = decoded;
            y++;
        }

        // Los proximos N + 1 caracteres son los punteros (a otros nodos)
        // { N = this->tamanio }
        y = 0;
        posicion = limite;
        limite += (this->tamanio + 1) * 4;
        for (x = posicion; x < limite; x += 4) {
            chunk[0] = datos_a_leer[x + 0];
            chunk[1] = datos_a_leer[x + 1];
            chunk[2] = datos_a_leer[x + 2];
            chunk[3] = datos_a_leer[x + 3];

            decoded = decode_int(chunk);
            this->punteros[y] = decoded;
            y++;
        }
    }

    void guardar() {
        string archivo = this->padre->getArchivo();
        unsigned char * data = this->serialize().data();
        int tamanio = this->totalSizeSerialized();
        int posicion = this->padre->getPadding() + this->indice * tamanio;

        escribir_al_archivo<unsigned char *>(archivo, data, tamanio, posicion);
    }

    void cargar() {
        string archivo = this->padre->getArchivo();
        int tamanio = this->totalSizeSerialized();
        unsigned char * datos_a_leer = new unsigned char[tamanio];
    
        int posicion = this->padre->getPadding() + this->indice * tamanio;

        leer_del_archivo<unsigned char *>(archivo, datos_a_leer, tamanio, posicion);
        this->deserialize(datos_a_leer);
    }

    void cargar(int index) {
        this->indice = index;
        this->cargar();
    }

    bool tiene_espacio() {
        // Si el ultimo de los valores tiene es distinto a 0, signfica que ya no hay espacio
        return (valores[tamanio - 1] == 0);
    }

    bool es_hoja() {
        // Si el primer puntero es 0 significa que es hoja
        return (punteros[0] == 0);
    }

    bool es_raiz() {
        // determina si el nodo es raiz
        return (this->padre->raiz->indice == this->indice);
    }

    int indice_del_valor(int valor) {
        int totalLimites = this->tamanio + 3;
        int* limites = new int[totalLimites];
        limites[0] = INT_MIN;
        for (int x = 0; x < this->tamanio + 1; x++) {
            limites[x + 1] = this->valores[x];
        }
        limites[this->tamanio + 1] = INT_MAX;

        // cout << "Buscando: " << valor << endl;
        for (int x = 0; x < totalLimites; x++) {
            int menor = limites[x];
            int mayor = limites[x + 1];

            if (valor > menor && (valor < mayor || mayor == 0) && menor != 0) {
                //cout<< "puntero " << x << ": " << this->punteros[x] 
                //    << " valores [ " << menor << ", " << mayor << " ]"
                //    << endl;
                return x;
            }
        }

        return -1;
    }

    int agregar(int valor, int direccion) {
        for (int x = 0; x < tamanio; x++) {
            if (valores[x] == 0) {
                valores[x] = valor;
                direcciones[x] = direccion;
                return 0;
            }

            if (valor == valores[x]) {
                return -1;
            }

            if (valor < valores[x]) {
                correr_arreglo(valores, x, tamanio);
                correr_arreglo(direcciones, x, tamanio);
                valores[x] = valor;
                direcciones[x] = direccion;
                return 0;
            }
        }

        return 0;
    }
};



#endif

