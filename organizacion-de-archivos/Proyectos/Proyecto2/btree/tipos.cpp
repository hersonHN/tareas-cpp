
#ifndef __TIPOS_CPP__  
#define __TIPOS_CPP__ 

class BTreeNode;

struct Respuesta {
    int estado;
    int indice_a_promover;
    int direccion_a_promover;
    BTreeNode* nodo_creado;
    BTreeNode* nodo_modificado;
};

struct NodosProcesados {
    BTreeNode * izquierdo;
    BTreeNode * derecho;
    BTreeNode * padre;
    int valor_a_promover;
    int direccion_a_promover;
    int posicion_de_valor_a_promover;
    int mitad;
    bool error;
};

struct Header {
    int indice_raiz;
    int tamanio_nodos;
    int cantidad_de_nodos;
};

#endif
