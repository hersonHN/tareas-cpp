
// Funciones Especificicas para la base de datos
// Crear archivos con formato, leer y escribir registros

#ifndef DB_CPP
#define DB_CPP

#include "classes/registro.cpp"
#include "classes/fecha.cpp"
#include "io.cpp"



// Declaracion de Funciones

void nueva_db        (const string&, int);
void agregar_registro(const string&, Registro&);
void leer_informacion(const string&);

int leer_arbol (int);

// Cuerpo de Funciones

void nueva_db(const string& archivo, int max_registros) {
    int posicion = 0;

    crear_archivo(__nombre_index__);
    crear_archivo(archivo);

    arbol = new BTree(TAMANIO, __nombre_index__);
    arbol->guardar_header();

    if (TAMANIO < 4) TAMANIO = 4;
    /*
    // Creado cuerpo del archivo
    Registro registro;
    for (int x = 0; x < max_registros * buckets_por_registro; x++) {
        Registro registro = { 0, 0, "", {0, 0, 0}, 0.0 };

        escribir<Registro>(archivo, registro, posicion);
        posicion += sizeof(registro);
    }
    */
}


void agregar_registro(const string& archivo, Registro& registro) {
    
    arbol = new BTree(TAMANIO, __nombre_index__);
    arbol->cargar_header();
    int cant = arbol->cantidad_de_nodos;
    int tamanio_reg = sizeof(Registro);
    int posicion_registro = tamanio_reg * cant;
    
    arbol->agregar(registro.id, posicion_registro);

    int id_a_escribir, id_escrito;
    Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
    leer<Registro>(archivo, registro_escrito, posicion_registro);

    // si está vacio, entonces lo escribe ahí
    if (registro_escrito.activo == 0 || registro.id == registro_escrito.id) {
        escribir<Registro>(archivo, registro, posicion_registro);
        return;
    }
}


void buscar_registro(const string& archivo, int id) {
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
    
    int tamanio_reg  = sizeof(registro);

    bool encontrado = false;
    arbol = new BTree(TAMANIO, __nombre_index__);
    arbol->cargar_header(); 
    int posicion = arbol->buscar(id);
    
    leer<Registro>(archivo, registro_escrito, posicion);

    if (registro_escrito.activo == 1 && registro_escrito.id == id) {
        encontrado = true;
        registro_escrito.activo = 0;
        cout << "Registro Encontrado:" << endl;
        cout << "   id     = " << registro_escrito.id << endl;
        cout << "   nombre = " << registro_escrito.nombre << endl;
        Fecha fecha = Fecha(
                registro_escrito.fecha_nacimiento[2],
                registro_escrito.fecha_nacimiento[1],
                registro_escrito.fecha_nacimiento[0]
        );
        cout << "   fecha  = " << fecha.printCorto() << endl;
        cout << "   sueldo = " << registro_escrito.sueldo << endl;
        return;
    }
    
    if (!encontrado) {
        cout << "Registro " << id << " no existe" << endl;
    }
}


void borrar_registro(const string& archivo, int id) {
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
    
    int tamanio_reg  = sizeof(registro);

    bool encontrado = false;
    arbol = new BTree(TAMANIO, __nombre_index__);
    arbol->cargar_header(); 
    int posicion = arbol->buscar(id);
    
    leer<Registro>(archivo, registro_escrito, posicion);

    if (registro_escrito.activo == 1 && registro_escrito.id == id) {
        encontrado = true;
        registro_escrito.activo = 0;
        escribir<Registro>(archivo, registro_escrito, posicion);
        cout << "Registro Borrado:" << endl;
        cout << "   id     = " << registro_escrito.id << endl;
        cout << "   nombre = " << registro_escrito.nombre << endl;
        Fecha fecha = Fecha(
                registro_escrito.fecha_nacimiento[2],
                registro_escrito.fecha_nacimiento[1],
                registro_escrito.fecha_nacimiento[0]
        );
        cout << "   fecha  = " << fecha.printCorto() << endl;
        cout << "   sueldo = " << registro_escrito.sueldo << endl;
        return;
    }
    
    if (!encontrado) {
        cout << "Registro " << id << " no existe" << endl;
    }
}

void leer_informacion(const string& archivo) {
    arbol->cargar_header();

    cout << "Total de registros: " << arbol->cantidad_de_nodos << endl;
    cout << "Orden del arbol: " << arbol->getTamanio() << endl;
}


void mostrar_estado(const string& archivo) {
    arbol->cargar_header();
    arbol->raiz->r_dump();
}


int leer_arbol (int id_registo) {
    return arbol->buscar(id_registo);
}

#endif

