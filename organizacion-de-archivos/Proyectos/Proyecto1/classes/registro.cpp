
#ifndef REGISTRO_CPP
#define REGISTRO_CPP

struct Registro {
    int activo;
    int id;
    char nombre[60];
    int fecha_nacimiento[3];
    double sueldo;
};

#endif
