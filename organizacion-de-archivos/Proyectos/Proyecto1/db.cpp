
// Funciones Especificicas para la base de datos
// Crear archivos con formato, leer y escribir registros

#ifndef DB_CPP
#define DB_CPP

#include <string.h>
#include "classes/header.cpp"
#include "classes/registro.cpp"
#include "io.cpp"
#include "classes/fecha.cpp"

#define FIRMA "MakeItRain"

// Declaracion de Funciones

void nueva_db        (const string&, int, int);
void agregar_registro(const string&, Registro&);
void leer_informacion(const string&);

Header leer_header     (const string&);
void actualizar_header (const string&, int);
int f_hash (int, int);

// Cuerpo de Funciones

void nueva_db(const string& archivo, int max_registros, int buckets_por_registro) {
    int posicion;

    
    // Creando archiv en blanco
    crear_archivo(archivo);
    
    // Creando Header
    posicion = 0;
    Header header = { "", max_registros, 0, buckets_por_registro, FIRMA };
    strcpy(header.db_name, archivo.c_str());
    escribir<Header>(archivo, header, 0);

    // Creado cuerpo del archivo
    posicion += sizeof(header);
    Registro registro;
    for (int x = 0; x < max_registros * buckets_por_registro; x++) {
        Registro registro = { 0, 0, "", {0, 0, 0}, 0.0 };

        escribir<Registro>(archivo, registro, posicion);
        posicion += sizeof(registro);
    }
}


void agregar_registro(const string& archivo, Registro& registro) {
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
        
    int buckets = header.buckets;
    int max_registros = header.limite_registros;
    int tamanio_reg  = sizeof(registro);
    int tamanio_head = sizeof(header);
    
    int posicion_registro = f_hash(registro.id, max_registros);

    // cout << "Posicion Registro: " << posicion_registro << endl;
    int posicion = (posicion_registro * tamanio_reg) * buckets;
    posicion += tamanio_head;
    // cout << "Posicion en bytes: " << posicion << endl;

    // Se desplaza el registro cuando está en un lugar ocupado
    int contador = max_registros - posicion_registro;
    while(contador-- > 0) {
        int id_a_escribir, id_escrito;
        Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
        // cout << "Posicion en bytes: " << posicion << endl;
        leer<Registro>(archivo, registro_escrito, posicion);

        // si está vacio, entonces lo escribe ahí
        if (registro_escrito.activo == 0 || registro.id == registro_escrito.id) {
            escribir<Registro>(archivo, registro, posicion);
            actualizar_header(archivo, 1);
            return;
        }

        posicion += tamanio_reg;
    }
}


void buscar_registro(const string& archivo, int id) {
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
        
    int buckets = header.buckets;
    int max_registros = header.limite_registros;
    int tamanio_reg  = sizeof(registro);
    int tamanio_head = sizeof(header);
    
    int posicion_registro = f_hash(id, max_registros);

    // cout << "Posicion Registro: " << posicion_registro << endl;
    int posicion = (posicion_registro * tamanio_reg) * buckets;
    posicion += tamanio_head;
    
    bool encontrado = false;
    int contador = max_registros - posicion_registro;
    
    while(contador-- > 0) {
        int id_a_escribir, id_escrito;
        Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
        leer<Registro>(archivo, registro_escrito, posicion);

        if (registro_escrito.activo == 1 && registro_escrito.id == id) {
            encontrado = true;
            registro_escrito.activo = 0;
            cout << "Registro Encontrado:" << endl;
            cout << "   id     = " << registro_escrito.id << endl;
            cout << "   nombre = " << registro_escrito.nombre << endl;
            Fecha fecha = Fecha(
                    registro_escrito.fecha_nacimiento[2],
                    registro_escrito.fecha_nacimiento[1],
                    registro_escrito.fecha_nacimiento[0]
            );
            cout << "   fecha  = " << fecha.printCorto() << endl;
            cout << "   sueldo = " << registro_escrito.sueldo << endl;
            return;
        }

        posicion += tamanio_reg;
    }
    
    if (!encontrado) {
        cout << "Registro " << id << " no existe" << endl;
    }
}


void borrar_registro(const string& archivo, int id) {
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
        
    int buckets = header.buckets;
    int max_registros = header.limite_registros;
    int tamanio_reg  = sizeof(registro);
    int tamanio_head = sizeof(header);
    
    int posicion_registro = f_hash(id, max_registros);

    // cout << "Posicion Registro: " << posicion_registro << endl;
    int posicion = (posicion_registro * tamanio_reg) * buckets;
    posicion += tamanio_head;
    
    bool borrado = false;
    int contador = max_registros - posicion_registro;
    while(contador-- > 0) {
        int id_a_escribir, id_escrito;
        Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
        leer<Registro>(archivo, registro_escrito, posicion);

        if (registro_escrito.activo == 1 && registro_escrito.id == id) {
            borrado = true;
            registro_escrito.activo = 0;
            cout << "Borrando registro con " << endl;
            cout << "   id     = " << registro_escrito.id << endl;
            cout << "   nombre = " << registro_escrito.nombre << endl;

            escribir<Registro>(archivo, registro_escrito, posicion);
            return;
        }
        
        posicion += tamanio_reg;
    }

    if (!borrado) {
        cout << "El registro no " << id << " existe" << endl;
    }
}


void actualizar_header(const string& archivo, int cantidad) {
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
    
    header.registros_activos = header.registros_activos + cantidad;
    escribir<Header>(archivo, header, 0);
}


void leer_informacion(const string& archivo) {
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
       
    cout << "Limite de registros: "    << header.limite_registros  << endl;
    cout << "Registros ocupados: "     << header.registros_activos << endl;
    cout << "Cantidad de buckets por posicion: " << header.buckets << endl;
}


void mostrar_estado(const string& archivo) {
    Header header = leer_header(archivo);
    if ( string(header.firma).compare(string(FIRMA)) ) return;
    
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    
    cout << "[O] = ocupado" << endl;
    cout << "[*] = borrado" << endl;
    cout << "[ ] = libre" << endl;
        
    int buckets = header.buckets;
    int max_registros = header.limite_registros;
    int tamanio_reg  = sizeof(registro);
    int tamanio_head = sizeof(header);
    
    int posicion_registro = 0;

    // cout << "Posicion Registro: " << posicion_registro << endl;
    int posicion = (posicion_registro * tamanio_reg) * buckets;
    posicion += tamanio_head;
    
    for (int y = 0; y < max_registros; y++) {
    cout <<  y  << ")";
    for (int x = 0; x < buckets; x++) {
        
        Registro registro_escrito = { 0, 0, "", {0, 0, 0}, 0.00 };
        leer<Registro>(archivo, registro_escrito, posicion);

        if (registro_escrito.activo == 1) {
            cout << " [O]";
        } else 
        if (x == 0 && y == 0) {
            if (string(registro_escrito.nombre).compare("")) {
                cout << " [*]";
            } else {
                cout << " [ ]";
            }
        } else
        if (registro_escrito.id == 0) {
            cout << " [ ]";
        } else {
            cout << " [*]";
        }

        posicion += tamanio_reg;

    }
    cout << endl;
    }
}

Header leer_header(const string& archivo) {
    Header header = { "", 0, 0, 0, "" };
    leer<Header>(archivo, header, 0);
    
    if ( string(header.firma).compare(string(FIRMA)) ) {
        cout << "Archivo de formato invalido" << endl;
    }

    return header;
}


int f_hash (int id_registo, int max_registros) {
    return id_registo % max_registros;
}

#endif

