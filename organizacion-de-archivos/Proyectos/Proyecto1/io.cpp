// Abstrae las funciones de Lectura/Escritura del archivo de base de datos, de modo que
// no se haga referencia a el fuera de estas funciones.

#ifndef IO_CPP
#define IO_CPP

#include <iostream>
#include <fstream>

#include "classes/header.cpp"
#include "classes/registro.cpp"

using namespace std;

// Declaracion de Funciones
                         void crear_archivo (const string&);
template <typename TIPO> void escribir      (const string&, TIPO&, int);
template <typename TIPO> void leer          (const string&, TIPO&, int);
 

// Cuerpo de Funciones

template <typename TIPO>
void escribir(const string& archivo, TIPO& data, int posicion) {
    // si el archivo no existe se crea
    ofstream out(archivo.c_str(), ios::in | ios::out | ios::binary | ios::ate);
    out.seekp(posicion);
    
    // escribir el struct en formato de un arreglo de caracteres
    out.write(reinterpret_cast<char*>(&data), sizeof(TIPO));
    out.close();
}

template <typename TIPO>
void leer(const string& archivo, TIPO& data, int posicion) {
    // intenta leer el archivo especificado
    ifstream in(archivo.c_str());
    in.seekg(posicion);

    // leer el struct en formato de un arreglo de caracteres
    // si el archivo no existe el struct no se sobreescribe
    in.read(reinterpret_cast<char*>(&data), sizeof(TIPO));
    in.close();
}

void crear_archivo(const string& archivo) {
    ofstream out(archivo.c_str());
}

#endif


