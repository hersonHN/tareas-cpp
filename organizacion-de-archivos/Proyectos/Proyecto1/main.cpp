
#include <iostream>
#include "db.cpp"
#include "classes/fecha.cpp"

using namespace std;

void menu(bool);

void opcion_crear();
void opcion_informacion();
void opcion_agregar();
void opcion_buscar();
void opcion_borrar();
void opcion_estado();

string nombre_del_archivo();
int getID();

string __nombre__ = "";

int main () {
    menu(true);
    return 0;
}


void menu(bool primeraVez) {
    if (primeraVez) {
        cout << "Bienvenido a su gestor de base de datos favorito!" << endl;
        cout << "=================================================" << endl;
    } else {
        string foo;
        cout << endl << "Escriba cualquier cosa para continuar: ";
        cin >> foo;

        cout << endl << endl << endl;
    }

    cout << "Elija una opcion: " << endl;
    cout << "1) Crear Base de Datos" << endl;
    cout << "2) Abrir y mostrar datos generales de la base de datos " << endl;
    cout << "3) Ingresar registro" << endl;
    cout << "4) Buscar registro" << endl;
    cout << "5) Borrar registro" << endl;
    cout << "6) Mostrar estado de cada registro" << endl;
    cout << "cualquier otro) Salir" << endl;
    
    char opt;
    cin >> opt;
    cout << endl << endl;

    if (opt == '1') { opcion_crear();       menu(0); }
    if (opt == '2') { opcion_informacion(); menu(0); }
    if (opt == '3') { opcion_agregar();     menu(0); }
    if (opt == '4') { opcion_buscar();      menu(0); }
    if (opt == '5') { opcion_borrar();      menu(0); }
    if (opt == '6') { opcion_estado();      menu(0); }
}

void opcion_crear() {
    string nombre;
    char respuesta;
    int max_registros, buckets;
    
    cout << "CREANDO NUEVA BASE DE DATOS" << endl;

    nombre = nombre_del_archivo();
    
    cout << "Cantidad maxima de registros: ";
    cin >> max_registros;
    
    cout << "Desea utilizar buckets? (y/n): ";
    cin >> respuesta;
    
    if (respuesta == 'y') {
        cout << "Cantidad de buckets a usar: ";
        cin >> buckets;

    } else {
        buckets = 1;
    }

    if (buckets < 1) buckets = 1;
    if (buckets > 9) buckets = 9;

    nueva_db(nombre, max_registros, buckets);
}


void opcion_informacion() {
    cout << "LEYENDO INFROMACION DE BASE DE DATOS" << endl;
    
    string nombre = nombre_del_archivo();
    cout << endl;
    leer_informacion(nombre);
}


void opcion_agregar() {
    string archivo = nombre_del_archivo();
    string nom;
    int dia, mes, anio;
    Registro registro = { 0, 0, "", {0, 0, 0}, 0.00 };
    registro.activo = 1;
    
    registro.id = getID();
    
    cout << "Escriba el nombre del empleado: ";
    cin >> registro.nombre;
    cin.getline(registro.nombre, sizeof(registro.nombre) - 1);
    
    cout << "Escriba la fecha de nacimiento: " << endl;
    cout << "Dia: ";  cin >> dia;
    cout << "Mes: ";  cin >> mes;
    cout << "Anio: "; cin >> anio;
    Fecha fecha = Fecha(anio, mes, dia);
    dia = fecha.getDia();
    mes = fecha.getMes();
    anio= fecha.getAnio();
    registro.fecha_nacimiento[0] = dia;
    registro.fecha_nacimiento[1] = mes;
    registro.fecha_nacimiento[2] = anio;
    
    cout << "Escriba el sueldo: ";
    cin >> registro.sueldo;
    
    agregar_registro(archivo, registro);
}


void opcion_borrar() {
    string archivo = nombre_del_archivo();
    int id = getID();
    
    borrar_registro(archivo, id);
}


void opcion_buscar() {
    string archivo = nombre_del_archivo();
    int id = getID();
    
    buscar_registro(archivo, id);
}


void opcion_estado() {
    string archivo = nombre_del_archivo();
    mostrar_estado(archivo);
}


string nombre_del_archivo() {
    if (__nombre__.length() > 0) {
        cout << __nombre__ << endl;
        return __nombre__;
    }
    
    char nombre[1000];
    do {
        cout << "Escriba el nombre de la base de datos: " << endl;
        cin.getline(nombre, sizeof(nombre) - 1);
    } while (string(nombre).length() == 0);

    __nombre__ = string(nombre);
    return __nombre__;
}

int getID() {
    int id;
    cout << "Escriba el id numerico: ";
    cin >> id;
    
    return id;
}

