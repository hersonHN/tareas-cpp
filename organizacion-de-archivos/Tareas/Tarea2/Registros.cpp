#include <iostream>
#include <fstream>

using namespace std;

struct Persona {
    char nombre[50];
    int edad;
    float sueldo;
};

struct Header {
    long length;
};

template <typename TIPO> void escribir(const string&, TIPO&, int);
template <typename TIPO> void     leer(const string&, TIPO&, int);

void crear_archivo(const string&);
void guardar_cantidad(string, long);
long leer_cantidad(string);
void agregar_persona(string, string, int, float);

void leer_registros(const string&, const string&);

void menu(const string&);


int main() {
    string archivo = "test.data";

    long cantidad_registros = leer_cantidad(archivo);
    cout << "Total de Registros: " << cantidad_registros << endl;

    menu(archivo);
}

void menu(const string& archivo) {

    char opcion;
    do {
        cout << endl;
        cout << "Seleccione Una Opcion:" << endl;
        cout << "1) Ver Todos Los Registros" << endl;
        cout << "2) Agregar Un Nuevo Registro" << endl;
        cout << "3) Buscar Por Nombre" << endl;
        cout << "0) Salir" << endl;
        
        cin >> opcion;
        
        cin.ignore(256, '\n');
        
        if (opcion == '1') {
            leer_registros(archivo, "");
        }

        if (opcion == '2') {
            char nombre[50];
            int edad; 
            float sueldo;
            
            cout << "Escriba el nombre: ";
            cin.get(nombre, 50);

            cout << "Ingrese la edad: ";
            cin >> edad;
            
            cout << "Ingrese el sueldo: ";
            cin >> sueldo;

            agregar_persona(archivo, string(nombre), edad, sueldo);
        }

        if (opcion == '3') {
            char busqueda[20];
            cout << "Ingrese porcion de la busqueda: ";
            cin.get(busqueda, 20);

            leer_registros(archivo, string(busqueda));
        }

    } while (opcion != '0');


}

template <typename TIPO>
void escribir(const string& archivo, TIPO& data, int inicio) {
    ofstream out(archivo.c_str(), ios::in | ios::out | ios::binary | ios::ate);
    out.seekp(inicio);
    out.write(reinterpret_cast<char*>(&data), sizeof(TIPO));
    out.close();
}

template <typename TIPO>
void leer(const string& archivo, TIPO& data, int inicio) {
    ifstream in(archivo.c_str());
    in.seekg(inicio);
    in.read(reinterpret_cast<char*>(&data), sizeof(TIPO));
    in.close();
}


void crear_archivo(const string& archivo) {
    ofstream out(archivo.c_str());
}

void leer_registros(const string& archivo, const string& busqueda) {
    long cantidad_registros = leer_cantidad(archivo);
    
    for (long n = 0; n < cantidad_registros; n++) {
        Persona persona = { "", 0, 0 };
        leer<Persona>(archivo, persona, sizeof(Header) + ( sizeof(Persona) * n ));
        
        if (strstr(persona.nombre, busqueda.c_str()) || busqueda.size() == 0) {
            cout << "Nombre: " << persona.nombre << ", "
                 << "Edad: "   << persona.edad   << ", "
                 << "Sueldo: " << persona.sueldo << endl;
        } 
    }
}



long leer_cantidad(string archivo) {
    Header header = { 0 };
    leer<Header>(archivo, header, 0);

    return header.length;
}

void agregar_persona(string archivo, string nombre, int edad, float sueldo) {
    long cantidad = leer_cantidad(archivo);
    
    if (cantidad == 0) {
        crear_archivo(archivo);
    }

    Header nuevo_header = { cantidad + 1 };
    escribir<Header>(archivo, nuevo_header, 0);

    char char_nombre[50];
    strcpy(char_nombre, nombre.c_str());
    
    Persona persona = {};
    strcpy(persona.nombre, char_nombre);
    persona.edad = edad;
    persona.sueldo = sueldo;

    escribir<Persona>(archivo, persona, sizeof(Header) + (sizeof(Persona) * cantidad));

}



